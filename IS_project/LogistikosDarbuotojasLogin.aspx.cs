﻿using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IS_project.Shared_class;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;
using System;

namespace IS_project
{
    public partial class LogistikosDarbuotojasLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {

            }
            else
            {
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Session["LogistikosDarbuotojas"] = Ricardo_class.LogistikosDarbuotojas.Prisijungti(t1.Text, t2.Text);
            Response.Redirect("LogistikosDarbuotojas.aspx", false);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["LogistikosDarbuotojas"] = null;
        }
        protected void Session_End(object sender, EventArgs e)
        {

        }
    }
}