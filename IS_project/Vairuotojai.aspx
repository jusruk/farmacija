﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Vairuotojai.aspx.cs" Inherits="IS_project.Vairuotojai" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>

    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="Vairuotojai.aspx">Farmacijos sistema</a>
                </div>
                <ul class="nav navbar-nav">
                    <li>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>
                </ul>
            </div>
        </nav>

        <div class="container">
  <h3><center>Profilio informacija</center></h3>
  <center><p>vardas:<asp:Label ID="Label1" runat="server" Text=""></asp:Label></center>
            </p>
</div>
        <p>

          <center><h3> Priskirtas uzsakymas</h3><asp:GridView ID="GridView1" runat="server" onrowdatabound="GridView1_RowDataBound">
        </asp:GridView>
              </center></p>


        </center>
        <center><h3>Priskirto uzsakymo tiekejai</h3></center><br />
        <center><asp:GridView ID="GridView3" runat="server" onrowdatabound="GridView3_RowDataBound">
        </asp:GridView></center>

        <center>
        <br />
            <br />
            <br />

        </center>
        <center><h3>Priskirtas transportas</h3></center><br />
        <center><asp:GridView ID="GridView2" runat="server" onrowdatabound="GridView2_RowDataBound">
        </asp:GridView></center>
    </form>
</body>
</html>
