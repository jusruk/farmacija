﻿using IS_project.Shared_class;
using IS_project.Edvino_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace IS_project
{
    public partial class Inventorius : System.Web.UI.Page
    {
        DBConnect db = new DBConnect();
        string patalpa;
        protected void Page_Load(object sender, EventArgs e)
        {
            //  code
            hl.NavigateUrl = "Inventorius.aspx";
            hl2.NavigateUrl = "Patalpos.aspx";
            hl3.NavigateUrl = "Tvarkarastis.aspx";
            hl4.NavigateUrl = "PrSrautai.aspx";

            if (!IsPostBack)
            {
                DropDownList2.Items.Insert(0, new ListItem("-Pasirinkite metus-", "0"));
                DropDownList3.Items.Insert(0, new ListItem("-Pasirinkite menesį-", "0"));
                DropDownList4.Items.Insert(0, new ListItem("-Pasirinkite dieną-", "0"));

                DropDownList5.Items.Insert(0, new ListItem("-Pasirinkite metus-", "0"));
                DropDownList6.Items.Insert(0, new ListItem("-Pasirinkite menesį-", "0"));
                DropDownList7.Items.Insert(0, new ListItem("-Pasirinkite dieną-", "0"));

                using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM patalpa"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = db.connection;
                    db.connection.Open();
                    DropDownList1.DataSource = cmd.ExecuteReader();
                    DropDownList1.DataTextField = "paskirtis";
                    DropDownList1.DataValueField = "id_Patalpa";
                    DropDownList1.DataBind();
                    DropDownList1.Items.Insert(0, new ListItem("pasirinkite patalpa", "0"));
                }

                for (int i = 0, j = 1950; j < DateTime.Now.Year; i++, j++)
                    DropDownList2.Items.Add(j.ToString());
                DropDownList3.Items.Add("Sausio");
                DropDownList3.Items.Add("Vasario");
                DropDownList3.Items.Add("Kovo");
                DropDownList3.Items.Add("Balandžio");
                DropDownList3.Items.Add("Gegužės");
                DropDownList3.Items.Add("Birželio");
                DropDownList3.Items.Add("Liepos");
                DropDownList3.Items.Add("Rugpjūčio");
                DropDownList3.Items.Add("Rugsėjo");
                DropDownList3.Items.Add("Spalio");
                DropDownList3.Items.Add("Lapkričio");
                DropDownList3.Items.Add("Gruodžio");
                DropDownList3.Enabled = false;
                DropDownList4.Enabled = false;


                for (int i = 0, j = 1950; j < DateTime.Now.Year; i++, j++)
                    DropDownList5.Items.Add(j.ToString());
                DropDownList6.Items.Add("Sausio");
                DropDownList6.Items.Add("Vasario");
                DropDownList6.Items.Add("Kovo");
                DropDownList6.Items.Add("Balandžio");
                DropDownList6.Items.Add("Gegužės");
                DropDownList6.Items.Add("Birželio");
                DropDownList6.Items.Add("Liepos");
                DropDownList6.Items.Add("Rugpjūčio");
                DropDownList6.Items.Add("Rugsėjo");
                DropDownList6.Items.Add("Spalio");
                DropDownList6.Items.Add("Lapkričio");
                DropDownList6.Items.Add("Gruodžio");
                DropDownList6.Enabled = false;
                DropDownList7.Enabled = false;

                Button1.Enabled = false;
                if (String.IsNullOrEmpty((string)Session["error"]))
                {
                    Label1.Visible = false;
                }
                else
                {
                    Label1.Visible = true;
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = (string)Session["error"];
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Register();
            Response.Redirect("Vadybininkai.aspx");
        }

        protected void Register()
        {
            Label1.Visible = true;
            Label1.ForeColor = System.Drawing.Color.Red;

            string prekes_kodas = TextBox1.Text;
            if (String.IsNullOrEmpty(prekes_kodas))
            {
                Session["error"] = "Prekės kodas turi būti įvestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string paskirtis = TextBox2.Text;
            if (String.IsNullOrEmpty(paskirtis))
            {
                Session["error"] = "Paskirtis turi būti įvesta";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string kiekis = TextBox3.Text;
            if (String.IsNullOrEmpty(kiekis) || int.Parse(kiekis) <=0)
            {
                Session["error"] = "Kiekis neįvestas arba įvestas neteisingai";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string bukle = TextBox4.Text;
            if (String.IsNullOrEmpty(bukle))
            {
                Session["error"] = "Būklė turi būti įvesta";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string date = DropDownList2.SelectedValue + "-" + (DropDownList3.SelectedIndex) + "-" + DropDownList4.SelectedValue;
            string gdata = DropDownList5.SelectedValue + "-" + (DropDownList6.SelectedIndex) + "-" + DropDownList7.SelectedValue;


            patalpa = DropDownList1.SelectedItem.Value.ToString();
            db.connection.Close();



            Edvino_class.Vadybininkas vadybinikas = (Edvino_class.Vadybininkas)Session["vadybininkas"];
            Edvino_class.Inventorius inventorius = new Edvino_class.Inventorius(prekes_kodas, date
                , gdata, paskirtis, int.Parse(kiekis), bukle, vadybinikas, patalpa);
            inventorius.Register();
            
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM patalpa"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = db.connection;
                db.connection.Open();
                DropDownList1.DataSource = cmd.ExecuteReader();
                DropDownList1.DataTextField = "paskirtis";
                DropDownList1.DataValueField = "id_Patalpa";
                DropDownList1.DataBind();
                DropDownList1.Items.Insert(0, new ListItem("pasirinkite patalpa", "0"));
            }

        }
        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            DropDownList3.Enabled = true;
        }
        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            switch (DropDownList3.SelectedValue)
            {
                case "Sausio":
                case "Kovo":
                case "Gegužės":
                case "Liepos":
                case "Rugpjūčio":
                case "Spalio":
                case "Gruodžio":
                    for (int i = 0; i < 31; i++)
                        DropDownList4.Items.Add((i + 1).ToString());
                    break;
                case "Balandžio":
                case "Birželio":
                case "Rugsėjo":
                case "Lapkričio":
                    for (int i = 0; i < 30; i++)
                        DropDownList4.Items.Add((i + 1).ToString());
                    break;
                case "Vasario":
                    if (int.Parse(DropDownList1.SelectedValue) % 4 == 0)
                        for (int i = 0; i < 29; i++)
                            DropDownList4.Items.Add((i + 1).ToString());
                    else
                        for (int i = 0; i < 28; i++)
                            DropDownList4.Items.Add((i + 1).ToString());
                    break;
            }
            DropDownList4.Enabled = true;
        }
        protected void DropDownList4_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }
        protected void DropDownList5_SelectedIndexChanged(object sender, EventArgs e)
        {

            DropDownList6.Enabled = true;
        }
        protected void DropDownList6_SelectedIndexChanged(object sender, EventArgs e)
        {

            switch (DropDownList6.SelectedValue)
            {
                case "Sausio":
                case "Kovo":
                case "Gegužės":
                case "Liepos":
                case "Rugpjūčio":
                case "Spalio":
                case "Gruodžio":
                    for (int i = 0; i < 31; i++)
                        DropDownList7.Items.Add((i + 1).ToString());
                    break;
                case "Balandžio":
                case "Birželio":
                case "Rugsėjo":
                case "Lapkričio":
                    for (int i = 0; i < 30; i++)
                        DropDownList7.Items.Add((i + 1).ToString());
                    break;
                case "Vasario":
                    if (int.Parse(DropDownList1.SelectedValue) % 4 == 0)
                        for (int i = 0; i < 29; i++)
                            DropDownList7.Items.Add((i + 1).ToString());
                    else
                        for (int i = 0; i < 28; i++)
                            DropDownList7.Items.Add((i + 1).ToString());
                    break;
            }
            DropDownList7.Enabled = true;
        }
        protected void DropDownList7_SelectedIndexChanged(object sender, EventArgs e)
        {

            Button1.Enabled = true;
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }

    }
}