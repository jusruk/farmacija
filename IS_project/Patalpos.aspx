﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Patalpos.aspx.cs" Inherits="IS_project.Patalpos" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
</head>
<body>
     <form id="form1" runat="server">
       <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="Vadybininkai.aspx">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="Vadybininkai.aspx">Home</a></li>
      <li><asp:HyperLink id="hl" runat="server" Text="Registruoti inventorių"/></li>
      <li class="active"><asp:HyperLink id="hl2" runat="server" Text="Priskirti patalpą"/></li>
        <li><asp:HyperLink id="hl3" runat="server" Text="Tvarkaraščiai"/></li>
        <li><asp:HyperLink id="hl4" runat="server" Text="Prekių srautų ataskaitos"/></li>
         <li>
           <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>
    </ul>
  </div>
</nav>
        <div class="container">
           <h2><center>Naujos patalpos panaudojimo priskyrimas</center></h2> <br />
            <br />
             <h3><asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></h3>
            <br />
             <div class="form-group">
                <div class="col-md-10">
            Plotas<br />
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width ="200px"></asp:TextBox>
                    </div>
                 </div>
            <br />
            <br />
              <div class="form-group">
                <div class="col-md-10">
            Paskirtis<br />
            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control"  Width="290px" Height="110px" TextMode="MultiLine"></asp:TextBox>
              </div>
                 </div>
                    <br />
            <br />
            <div class="form-group">
                <div class="col-md-10">
            Sildymo tipas<br />
            <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control"  Width="200px"></asp:TextBox>
                      </div>
                 </div>
            <br />
            <br />
            <div class="form-group">
                <div class="col-md-10">
            Signalizacija<br />
            <asp:DropDownList ID="DropDownList2" runat="server"  CssClass="form-control"  Width="200px" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
            </asp:DropDownList>
                       </div>
                 </div>
            <br />
            <br />
             <div class="form-group">
                <div class="col-md-10">
            Dumu detektorius<br />
            <asp:DropDownList ID="DropDownList3" runat="server" CssClass="form-control"  Width="200px" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
            </asp:DropDownList>
                       </div>
                 </div>
            <br />
            <br />
            <div class="form-group">
                <div class="col-md-10">
            Vaistine<br />
            <asp:DropDownList ID="DropDownList4" runat="server" CssClass="form-control"  Width="200px">
            <asp:ListItem Value="0">-- pasirinkite vaistine --</asp:ListItem> 
            </asp:DropDownList>
                        </div>
                 </div>
            <br />
            <br />
            <br />
            <br />
             <div class="form-group">
                <div class="col-md-10">
                    </br>
            <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" OnClick="Button1_Click" Text="Registruoti" />
                     </div>
                 </div>
            <br />
        </div>
    </form>
</body>
</html></html>
