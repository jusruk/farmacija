﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VaistininkaiRegister.aspx.cs" Inherits="IS_project.VaistininkasRegister" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
</head>
<body>
   <form id="form1" runat="server">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="Vaistininkai.aspx">Farmacijos sistema</a>
                </div>
                <ul class="nav navbar-nav">
                    <li ><a href="Vaistininkai.aspx">Home</a></li>
                    <li class="active"><asp:HyperLink id="hl" runat="server" Text="Registruoti vaistininką"/></li>
                    <li><asp:HyperLink id="hl1" runat="server" Text="Sudaryti sąskaitą"/></li>
                    <li><asp:HyperLink id="hl2" runat="server" Text="Teikti nuolaidą prekei"/></li>
                    <li><asp:HyperLink id="hl3" runat="server" Text="Susieti sąskaitą su mokėjimu"/></li>
                    <li><asp:HyperLink ID="hl4" runat="server" Text="Neapmokėtų sąskaitų ataskaita"/></li>
                    <li><asp:HyperLink ID="hl5" runat="server" Text="Permokėtų sąskaitų ataskaita"/></li>
                    <li>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <h2><center>Naujo vaistininko registracija</center></h2><br />
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            <br />
            <div class="form-group">
                <div class="col-md-10">
            Gimimo data<br />
            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control"  Width ="200px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control"  Width ="200px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList3" runat="server" CssClass="form-control"  Width ="200px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
            </asp:DropDownList>
          </div>
                </div>
              <div class="form-group">
                <div class="col-md-10">
            Tabelio numeris<br />
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width ="200px"></asp:TextBox>
                    </div>
                  </div>
            <br />
             <div class="form-group">
                <div class="col-md-10">
            Slaptažodis<br />
            <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"  CssClass="form-control" Width ="200px"></asp:TextBox>
                     </div>
                  </div>
            <br />

             <div class="form-group">
                <div class="col-md-10">
            Vardas<br />
            <asp:TextBox ID="TextBox3" runat="server"  CssClass="form-control" Width ="200px"></asp:TextBox>
            </div>
                  </div>
                    
                    <br />
                     <div class="form-group">
                <div class="col-md-10">
            Pavardė<br />
            <asp:TextBox ID="TextBox4" runat="server"  CssClass="form-control" Width ="200px"></asp:TextBox>
            </div>
                  </div>
                    
                    <br />
                     <div class="form-group">
                <div class="col-md-10">
            Banko sąskaita<br />
            <asp:TextBox ID="TextBox5" runat="server"  CssClass="form-control" Width ="200px"></asp:TextBox>
           </div>
                  </div>
                    
                    <br />
            <div class="form-group">
                <div class="col-md-10">
                    Telefono numeris<br />
            <asp:TextBox ID="TextBox6" runat="server"  CssClass="form-control" Width ="200px"></asp:TextBox>
             </div>
                  </div>
                    <br />
             <div class="form-group">
                <div class="col-md-10">
                    Elektroninis paštas<br />
            <asp:TextBox ID="TextBox7" runat="server"  CssClass="form-control" Width ="200px"></asp:TextBox>
             </div>
                  </div>
                    <br />
             <div class="form-group">
                <div class="col-md-10">
            Lytis<asp:RadioButtonList ID="RadioButtonList1" runat="server">
                <asp:ListItem Selected="True">Vyras</asp:ListItem>
                <asp:ListItem>Moteris</asp:ListItem>
            </asp:RadioButtonList>
                      </div>
                  </div>
            <br />
                     <div class="form-group">
                <div class="col-md-10">
            Darbo stažas<br />
            <asp:TextBox ID="TextBox8" runat="server"  CssClass="form-control" Width ="200px"></asp:TextBox>
             </div>
                  </div>
                    <br />
                    
                    <br />
            <br />
             <div class="form-group">
                <div class="col-md-10">
                       <br />
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" CssClass="btn btn-primary" Text="Registruoti" />
                       <br />
            <br />
                    </div>
                 </div>
         
            <br />

        </div>
    </form>
</body>
</html>
