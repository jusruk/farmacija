﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Vadybininkai.aspx.cs" Inherits="IS_project.Vadybininkai" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
    
    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
</head>
<body>
    <form id="form1" runat="server">
       <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="Vadybininkai.aspx">Farmacijos sistema</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="Vadybininkai.aspx">Home</a></li>
      <li><asp:HyperLink id="hl" runat="server" Text="Registruoti inventorių"/></li>
      <li><asp:HyperLink id="hl2" runat="server" Text="Teikti patalpas"/></li>
        <li><asp:HyperLink id="hl3" runat="server" Text="Sudaryti vizitų tvarkaraštį"/></li>
        <li><asp:HyperLink id="hl4" runat="server" Text="Stebėti prekių srautą"/></li>
       <li>
           <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>
    </ul>
  </div> 
</nav>

        <div class="container">
  <center><h3>Sveiki,</h3>
  <p><asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
</div>
        <center><u><h2>Sistemos informacija</h2></u> </center><br /><br />
        <center> <h3>Vaistinių informacija</h3></center>


        <center><asp:GridView ID="GridView3" runat="server" onrowdatabound="GridView3_RowDataBound">
        </asp:GridView></center>

           <br />

         <center> <h3>Patalpos</h3>
             <p>
                 <asp:GridView ID="GridView2" runat="server" onrowdatabound="GridView2_RowDataBound">
                 </asp:GridView>
             </p></center>
        <br />

       <center> <h3>Registruotas inventorius</h3></center>


        <center><asp:GridView ID="GridView1" runat="server" onrowdatabound="GridView1_RowDataBound">
        </asp:GridView></center>

        <br />
     

    </form>
</body>
</html>
