﻿using IS_project.Shared_class;
using IS_project.Justino_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS_project
{
    public partial class VaistininkasLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Session["Vaistininkas"] = Justino_class.Vaistininkas.Prisijungti(t1.Text, t2.Text);
            Response.Redirect("Vaistininkai.aspx", false);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["Vaistininkas"] = null;

        }
        protected void Session_End(object sender, EventArgs e)
        {

        }
    }
}