﻿using IS_project.Shared_class;
using IS_project.Justino_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS_project
{
    public partial class VairuotojaiLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Session["Vairuotojas"] = Ricardo_class.Vairuotojas.Prisijungti(t1.Text, t2.Text);
            Response.Redirect("Vairuotojai.aspx", false);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["Vairuotojas"] = null;

        }
        protected void Session_End(object sender, EventArgs e)
        {

        }
    }
}