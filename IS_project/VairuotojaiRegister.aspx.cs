﻿using IS_project.Shared_class;
using IS_project.Justino_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS_project
{
    public partial class VairuotojaiRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hl.NavigateUrl = "VairuotojaiRegister.aspx";
            hl2.NavigateUrl = "TransportoPriemoneRegister.aspx";
            hl3.NavigateUrl = "TransportoPriemoneAssign.aspx";
            hl4.NavigateUrl = "UzsakymaiAssign.aspx";
            if (!IsPostBack)
            {
                if (String.IsNullOrEmpty((string)Session["error"]))
                {
                    Label1.Visible = false;
                }
                else
                {
                    Label1.Visible = true;
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = (string)Session["error"];
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Register();

        }

        protected void Register()
        {
            Label1.Visible = true;
            Label1.ForeColor = System.Drawing.Color.Red;
            string vardas = TextBox1.Text;
            if (String.IsNullOrEmpty(vardas))
            {
                Session["error"] = "Vardas turi būti ivestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string pavarde = TextBox2.Text;
            if (String.IsNullOrEmpty(pavarde))
            {
                Session["error"] = "Pavarde turi būti ivesta";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string id_nr = TextBox3.Text;
            if (String.IsNullOrEmpty(id_nr))
            {
                Session["error"] = "Pazymejimo numeris turi buti ivesta";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string company = TextBox4.Text;
            if (String.IsNullOrEmpty(company))
            {
                Session["error"] = "Imones pavadinimas turi buti ivestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string telNr = TextBox5.Text;
            if (String.IsNullOrEmpty(telNr))
            {
                Session["error"] = "Telefono numeris turi buti ivestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string email = TextBox6.Text;
            if (String.IsNullOrEmpty(email))
            {
                Session["error"] = "Elektroninis pastas turi buti ivestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (email.Count((x) => x.Equals('@')) != 1)
            {
                Session["error"] = "Elektroninis paštas turi būti įvestas teisingai";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (!email.Substring(email.IndexOf('@')).Contains("."))
            {
                Session["error"] = "Elektroninis paštas turi būti įvestas teisingai";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string password = TextBox7.Text;
            if (String.IsNullOrEmpty(password))
            {
                Session["error"] = "Slaptazodis turi buti ivestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }

            Ricardo_class.Vairuotojas vairuotojas = new Ricardo_class.Vairuotojas
                (vardas, pavarde, id_nr, company, telNr, email, ((Ricardo_class.LogistikosDarbuotojas)Session["LogistikosDarbuotojas"]).TabelioNr);
            vairuotojas.Irasyti(password);
            Label1.ForeColor = System.Drawing.Color.Green;
            Session["error"] = "";
            Response.Redirect("LogistikosDarbuotojas.aspx");
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}