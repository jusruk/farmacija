﻿using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS_project
{
    public partial class AtaskaitaNeapmoketos : System.Web.UI.Page
    {
        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {
            hl.NavigateUrl = "VaistininkaiRegister.aspx";
            hl1.NavigateUrl = "SaskaitosCreate.aspx";
            hl2.NavigateUrl = "Nuolaidos.aspx";
            hl3.NavigateUrl = "SaskaitosAssign.aspx";
            hl4.NavigateUrl = "AtaskaitaNeapmoketos.aspx";
            hl5.NavigateUrl = "AtaskaitaPermoketos.aspx";
            if (((Justino_class.Vaistininkas)Session["vaistininkas"]) != null)
            {
                db.connection.Open();
                if (!GautiDuomenis())
                    Label1.Visible = true;
                else Label1.Visible = false;

                db.connection.Close();
            }
            else if (Request.UrlReferrer == null)
            {
                Response.Redirect("Pagrindinis.aspx");
            }
            else
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
        }

        protected bool GautiDuomenis()
        {
            string query = "SELECT klientas.vardas, klientas.pavarde, saskaita.suma AS saskaitos_suma, mokejimas.data, " +
                "prekiu_krepselis.suma AS krepselio_suma FROM klientas, saskaita, mokejimas, prekiu_krepselis " +
                "WHERE saskaita.fk_Prekiu_krepselisid = prekiu_krepselis.id AND mokejimas.fk_Saskaitaid = saskaita.id " +
                "AND klientas.id_klientas = mokejimas.fk_Klientasid_Klientas AND saskaita.apmoketa = 0 ";
            MySqlCommand cmd = new MySqlCommand(query, db.connection);
            cmd.CommandType = CommandType.Text;

            using (MySqlDataReader dr = cmd.ExecuteReader())
            {
                if (!dr.HasRows)
                    return false;
                GridView1.DataSource = dr;
                GridView1.DataBind();
            }
            return true;
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}