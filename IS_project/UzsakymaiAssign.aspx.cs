﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IS_project.Shared_class;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;
using IS_project.Ricardo_class;

namespace IS_project
{
    public partial class UzsakymaiAssign : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hl.NavigateUrl = "VairuotojaiRegister.aspx";
            hl2.NavigateUrl = "TransportoPriemoneRegister.aspx";
            hl3.NavigateUrl = "TransportoPriemoneAssign.aspx";
            hl4.NavigateUrl = "UzsakymaiAssign.aspx";
            if (!IsPostBack)
            {
                List<Uzsakymas> uzsakymas = Uzsakymas.uzsakymas();
                if (uzsakymas.Count != 0)
                {
                    if (String.IsNullOrEmpty((string)Session["error"]))
                    {
                        Label1.Visible = false;
                    }
                    else
                    {
                        Label1.Visible = true;
                        Label1.ForeColor = System.Drawing.Color.Red;
                        Label1.Text = (string)Session["error"];
                    }
                    DropDownList1.Items.Add("Pasirinkite uzsakyma");
                    foreach (var u in uzsakymas)
                        DropDownList1.Items.Add(u.UzsakymoID);
                }
                List<Vairuotojas> vairuotojas = Vairuotojas.vairuotojai2();
                if (vairuotojas.Count != 0)
                {
                    if (String.IsNullOrEmpty((string)Session["error"]))
                    {
                        Label1.Visible = false;
                    }
                    else
                    {
                        Label1.Visible = true;
                        Label1.ForeColor = System.Drawing.Color.Red;
                        Label1.Text = (string)Session["error"];
                    }
                    DropDownList2.Items.Add("Pasirinkite vairuotoja");
                    foreach (var v in vairuotojas)
                        DropDownList2.Items.Add(v.PazymejimoNr);
                }
            }
            else if (Uzsakymas.uzsakymas().Count == 0 || Vairuotojas.vairuotojai2().Count == 0)
            {
                Label1.Visible = true;
                Button1.Enabled = false;
            }
            else
            {
                Label1.Visible = false;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<Uzsakymas> uzsakymas = Uzsakymas.uzsakymas();
            List<Vairuotojas> vairuotojas = Vairuotojas.vairuotojai2();
            if (DropDownList1.SelectedIndex != 0 && DropDownList2.SelectedIndex != 0)
            {
                Uzsakymas u = uzsakymas[DropDownList1.SelectedIndex - 1];
                Vairuotojas v = vairuotojas[DropDownList2.SelectedIndex - 1];
                u.Priskirti(v);
                Response.Redirect("LogistikosDarbuotojas.aspx");
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}