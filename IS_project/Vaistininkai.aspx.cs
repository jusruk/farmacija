﻿using System;
using IS_project.Justino_class;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System.Data;

namespace IS_project
{
    public partial class Vaistininkas : System.Web.UI.Page
    {
        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (((Justino_class.Vaistininkas)Session["vaistininkas"]) != null)
            {
                Justino_class.Vaistininkas vaistininkas = ((Justino_class.Vaistininkas)Session["vaistininkas"]);
                Label1.Text = vaistininkas.Vardas + " " + vaistininkas.Pavarde;
                hl.NavigateUrl = "VaistininkaiRegister.aspx";
                hl1.NavigateUrl = "SaskaitosCreate.aspx";
                hl2.NavigateUrl = "Nuolaidos.aspx";
                hl3.NavigateUrl = "SaskaitosAssign.aspx";
                hl4.NavigateUrl = "AtaskaitaNeapmoketos.aspx";
                hl5.NavigateUrl = "AtaskaitaPermoketos.aspx";
                db.connection.Open();
                PildytiPrekes();
                PildytiSaskaitas();
                PildytiMokejimus();
                db.connection.Close();
            }
            else if (Request.UrlReferrer == null)
            {
                Response.Redirect("Pagrindinis.aspx");
            }
            else
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
        }

        protected void PildytiPrekes()
        {
            string query = "select id_Preke, pavadinimas, gamintojas, kiekis, kaina from preke";
            MySqlCommand cmd = new MySqlCommand(query, db.connection);
            cmd.CommandType = CommandType.Text;

            using (MySqlDataReader dr = cmd.ExecuteReader())
            {
                GridView1.DataSource = dr;
                GridView1.DataBind();
            }
        }

        protected void PildytiSaskaitas()
        {
            string query = "select id, suma, sudarymo_data, apmoketa, apmokejimo_data from saskaita";
            MySqlCommand cmd = new MySqlCommand(query, db.connection);
            cmd.CommandType = CommandType.Text;

            using (MySqlDataReader dr = cmd.ExecuteReader())
            {
                GridView2.DataSource = dr;
                GridView2.DataBind();
            }
        }

        protected void PildytiMokejimus()
        {
            string query = "select id_Mokejimas, suma, data from mokejimas";
            MySqlCommand cmd = new MySqlCommand(query, db.connection);
            cmd.CommandType = CommandType.Text;

            using (MySqlDataReader dr = cmd.ExecuteReader())
            {
                GridView3.DataSource = dr;
                GridView3.DataBind();
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }

    }
}