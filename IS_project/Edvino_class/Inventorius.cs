﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IS_project.Shared_class;
using IS_project.Edvino_class;
using MySql.Data.MySqlClient;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS_project.Edvino_class
{
    public class Inventorius
    {
        //code
        public string Prekes_kodas { get; private set; }
        public string Pagaminimo_data { get; private set; }
        public string Garantinio_data { get; private set; }
        public string Paskirtis { get; private set; }
        public int Kiekis { get; private set; }
        public string Bukle { get; private set; }
        public Vadybininkas vadybinikas { get; private set; }
        public string patalpa { get; private set; }

        public Inventorius(string Prekes_kodas, string Pagaminimo_data, string Garantion_data, string Paskirtis, int Kiekis,
                           string Bukle, Vadybininkas vad, string patalpa)
        {
            this.Prekes_kodas = Prekes_kodas;
            this.Pagaminimo_data = Pagaminimo_data;
            this.Garantinio_data = Garantion_data;
            this.Paskirtis = Paskirtis;
            this.Kiekis = Kiekis;
            this.Bukle = Bukle;
            this.vadybinikas = vad;
            this.patalpa = patalpa;
        }

        public void Register()
        {
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "INSERT INTO inventorius" +
                " (`pagaminimo_data`, `garantinio_data`, `paskirtis`, `prekes_kodas`, `kiekis`, `bukle`, `fk_Vadybininkasid_Vadybininkas`,`fk_Patalpaid_Patalpa`)" +
                "VALUES ('" +
                Pagaminimo_data + "', '" + Garantinio_data + "', '" +
                Paskirtis + "', '" + Prekes_kodas + "', '" + Kiekis +
                "', '" + Bukle + "', '" + vadybinikas.id + "', '" + patalpa + "')";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            db.connection.Close();   
        }

    }


}
