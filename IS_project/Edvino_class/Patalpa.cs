﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IS_project.Shared_class;
using IS_project.Edvino_class;
using MySql.Data.MySqlClient;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace IS_project.Edvino_class
{
    public class Patalpa
    {
        public double Plotas { get; private set; }
        public string Paskirtis { get; private set; }
        public string Sildymo_tipas { get; private set; }
        public int Signalizacija { get; private set; }
        public int Dumu_detektorius { get; private set; }
        public Vadybininkas vadybinikas { get; private set; }
        public List<Inventorius> inventorius;
        public int vaistine { get; private set; }

        public Patalpa(double plotas, string paskirtis, string sildymo_tipas, int signalizacija,
                       int dumu_detektorius, Vadybininkas vadyb, int vaistine)
        {
            this.Plotas = plotas;
            this.Paskirtis = paskirtis;
            this.Sildymo_tipas = sildymo_tipas;
            this.Signalizacija = signalizacija;
            this.Dumu_detektorius = dumu_detektorius;
            this.vadybinikas = vadyb;
            this.inventorius = new List<Inventorius>();
            this.vaistine = vaistine;
        }

        public void Register()
        {
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "INSERT INTO patalpa" +
                " (`plotas`, `paskirtis`, `sildymo_tipas`, `signalizacija`, `dumu_detektorius`, `fk_Vaistineid_Vaistine`, `fk_Vadybininkasid_Vadybininkas`)" +
                "VALUES ('" +
                Plotas + "', '" + Paskirtis + "', '" +
                Sildymo_tipas + "', '" + Signalizacija + "', '" + Dumu_detektorius +
                "', '" +vaistine  + "', '" + vadybinikas.id + "')";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            
        }

    }
}