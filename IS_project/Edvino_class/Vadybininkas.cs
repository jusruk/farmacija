﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IS_project.Edvino_class;
using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System.Data;

namespace IS_project.Edvino_class
{
    public class Vadybininkas
    {
        public string Tabelio_nr { get; private set; }
        public string Vardas { get; private set; }
        public string Pavarde { get; private set; }
        public string Tel_nr { get; private set; }
        public string El_pastas { get; private set; }
        public string Faksas { get; private set; }
        public string id { get; private set; }

        public Vadybininkas(string tabelio_nr, string vardas, string pavarde, string tel_nr,
                            string el_pastas, string faksas, string id)
        {
            this.Tabelio_nr = tabelio_nr;
            this.Vardas = vardas;
            this.Pavarde = pavarde;
            this.Tel_nr = tel_nr;
            this.El_pastas = el_pastas;
            this.Faksas = faksas;
            this.id = id;
        }

        public static Vadybininkas Prisijungti(string uname, string psw)
        {
            DBConnect db = new DBConnect();
            db.connection.Open();

            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from vadybininkas where tabelio_nr= '" + uname + "' and password='" + psw + "'";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            Vadybininkas v = null;
            foreach (DataRow dr in dt.Rows)
            {
                v = new Edvino_class.Vadybininkas(
                  dr["tabelio_nr"].ToString(),
                  dr["vardas"].ToString(),
                  dr["pavarde"].ToString(),
                  dr["tel_nr"].ToString(),
                  dr["el_pastas"].ToString(),
                  dr["faksas"].ToString(),
                  dr["id_vadybininkas"].ToString());
            }
            db.connection.Close();
            return v;
        }
        }
}