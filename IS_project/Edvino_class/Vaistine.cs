﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IS_project.Edvino_class;
using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System.Data;
namespace IS_project.Edvino_class
{
    public class Vaistine
    {
        public string Pavadinimas { get; private set; }
        public string Adresas { get; private set; }
        public string Telefonas { get; private set; }
        public string El_pastas { get; private set; }
        public string Faksas { get; private set; }
        public string Pasto_kodas { get; private set; }
        public string Miestas { get; private set; }
        public int Dydis { get; private set; }
        public List<Patalpa> Patalpos;
       
        public Vaistine(string pav, string adr, string tel, string el, string faks, string past, string miest, int dyd)
        {
            this.Pavadinimas = pav;
            this.Adresas = adr;
            this.Telefonas = tel;
            this.El_pastas =el;
            this.Faksas = faks;
            this.Pasto_kodas = past;
            this.Miestas = miest;
            this.Dydis = dyd;
            this.Patalpos = new List<Patalpa>();

        }

        public static List<Vaistine> GautiVaistines()
        {
            List<Vaistine> list = new List<Vaistine>();
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "SELECT * FROM vaistine";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Vaistine n = new Vaistine(dr["pavadinimas"].ToString(), dr["adresas"].ToString(),
                    dr["telefonas"].ToString(), dr["el_pastas"].ToString(), dr["faksas"].ToString(),
                    dr["pasto_kodas"].ToString(), dr["miestas"].ToString(), (int)dr["dydis"]);
                list.Add(n);
            }
            db.connection.Close();
            return list;
        }


    }
}