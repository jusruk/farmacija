﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VadybininkasLogin.aspx.cs" Inherits="IS_project.VadybininkasLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
        <link rel="stylesheet" href="css/style.css" />
    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
    <title>Login</title>
    <style type="text/css">
        .auto-style1 {
            padding-top: 150px;
            height: 30px;
        }

        .div {
            margin-top: 150px;
        }

        * {
            padding: auto;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }

        .container {
            margin-top: 50px;
            padding-top: auto;
            width: 43%;
            border-radius: 25px;
            background-color:rgb(255,215,0)
        }

        .btn-group-lg > .btn, .btn-lg {
            padding: 10px 16px;
            font-size: 18px;
            line-height: 1.3333333;
            border-radius: 6px;
            margin-bottom: 20px;
        }
    </style>
</head>
<body>
    <body>
        <form id="form2" runat="server">
            <div>
                <div class="container">
                    <h2>Vadybininko prisijungimas</h2>
                    <div class="container">
                        <label><b>Vardas</b></label>
                        <asp:TextBox ID="t1" CssClass="form-control input-lg" runat="server" TabIndex="1" Width="200px"></asp:TextBox>
                    </div>
                    
                    <div class="container">
                        <label>
                            <b>Slaptazodis</b>
                        </label>
                        <asp:TextBox ID="t2" CssClass="form-control input-lg" runat="server" TabIndex="2" Width="200px" Height="45px" TextMode="Password"></asp:TextBox>
                    </div>
                    <br />
                    <br />
                        <asp:Button ID="Button2" CssClass="btn btn-default btn-lg" runat="server" Text="Prisijungti" OnClick="Button2_Click"   /> 
                </div>
            </div>
        </form>
    </body>
</body>
</html>

