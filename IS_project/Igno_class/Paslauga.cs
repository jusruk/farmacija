﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IS_project.Igno_class
{
    public class Paslauga
    {
        public int ID { get; private set; }
        public double kaina { get; private set; }
        public string laikas { get; private set; }
        public int klientas { get; private set; }

        public Paslauga(int id, double kaina, string laikas, int klientas)
        {
            ID = id;
            this.kaina = kaina;
            this.laikas = laikas;
            this.klientas = klientas;
        }
    }
}