﻿using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IS_project.Igno_class
{
    public class PrekiuKrepselis
    {
        public string ID { get; private set; }
        public double suma { get; private set; }
        public int kiekis { get; private set; }
        public int? nuolaiduKortele { get; private set; }
        public int? apziura { get; private set; }
        public int? paslauga { get; private set; }
        public int? vaistininkas { get; private set; }
        public int? preke { get; private set; }

        public PrekiuKrepselis(string ID, double suma, int kiekis, int? nuolaida, int? apziura, int? paslauga, int? vaistininkas, int? preke)
        {
            this.ID = ID;
            this.suma = suma;
            this.kiekis = kiekis;
            nuolaiduKortele = nuolaida;
            this.apziura = apziura;
            this.paslauga = paslauga;
            this.vaistininkas = vaistininkas;
            this.preke = preke;
        }

        public static List<PrekiuKrepselis> Krepseliai()
        {
            List<PrekiuKrepselis> pkList = new List<PrekiuKrepselis>();
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "SELECT * FROM prekiu_krepselis";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                int? kortele = null;
                int? apziura = null;
                int? paslauga = null;
                int? vaistininkas = null;
                int? preke = null;
                if (!String.IsNullOrEmpty(dr["fk_Nuolaidu_korteleid_Nuolaidu_kortele"].ToString()))
                {
                    kortele = int.Parse(dr["fk_Nuolaidu_korteleid_Nuolaidu_kortele"].ToString());
                }
                if (!String.IsNullOrEmpty(dr["fk_Apziuraid_Apziura"].ToString()))
                {
                    apziura = int.Parse(dr["fk_Apziuraid_Apziura"].ToString());
                }
                if (!String.IsNullOrEmpty(dr["fk_Paslaugaid_Paslauga"].ToString()))
                {
                    paslauga = int.Parse(dr["fk_Paslaugaid_Paslauga"].ToString());
                }
                if (!String.IsNullOrEmpty(dr["fk_Vaistininkastabelio_nr"].ToString()))
                {
                    vaistininkas = int.Parse(dr["fk_Vaistininkastabelio_nr"].ToString());
                }
                if (!String.IsNullOrEmpty(dr["fk_Prekeid_Preke"].ToString()))
                {
                    vaistininkas = int.Parse(dr["fk_Prekeid_Preke"].ToString());
                }
                PrekiuKrepselis pk = new PrekiuKrepselis(dr["id"].ToString(),
                    double.Parse(dr["suma"].ToString()), int.Parse(dr["kiekis"].ToString()),
                    kortele, apziura, paslauga, vaistininkas, preke);
                pkList.Add(pk);
            }
            db.connection.Close();
            return pkList;
        }
    }
}