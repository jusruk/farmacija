﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IS_project.Igno_class
{
    public class Apziura
    {
        public int ID { get; private set; }
        public string laikas { get; private set; }
        public string data { get; private set; }
        public string tyrimas { get; private set; }
        public int klientas { get; private set; }

        public Apziura(int id, string laikas, string data, string tyrimas, int klientas)
        {
            ID = id;
            this.laikas = laikas;
            this.data = data;
            this.tyrimas = tyrimas;
            this.klientas = klientas;
        }
    }
}