﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IS_project.Igno_class
{
    public class Klientas
    {
        public int ID { get; private set; }
        public string vardas { get; private set; }
        public string pavarde { get; private set; }
        public string adresas { get; private set; }
        public string telNr { get; private set; }
        public string gimimo_data { get; private set; }
        public string ligos { get; private set; }

        public Klientas(int id, string vardas, string pavarde, string adresas, string telnr, string gimimodata,
            string ligos)
        {
            ID = id;
            this.vardas = vardas;
            this.pavarde = pavarde;
            this.adresas = adresas;
            telNr = telnr;
            gimimo_data = gimimodata;
            this.ligos = ligos;

        }
    }
}