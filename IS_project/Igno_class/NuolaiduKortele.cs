﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IS_project.Igno_class
{
    public class NuolaiduKortele
    {
        public int ID { get; private set; }
        public string data_nuo { get; private set; }
        public string data_iki { get; private set; }
        public int tipas { get; private set; }
        public double kaina { get; private set; }
        public int klientas { get; private set; }

        public NuolaiduKortele(int id, string nuo, string iki, int tipas, double kaina, int klientas)
        {
            ID = id;
            data_nuo = nuo;
            data_iki = iki;
            this.tipas = tipas;
            this.kaina = kaina;
            this.klientas = klientas;
        }
    }
}