﻿using IS_project.Justino_class;
using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IS_project.Igno_class
{
    public class Preke
    {
        public int ID { get; private set; }
        public string rusis { get; private set; }
        public string pavadinimas { get; private set; }
        public string gamintojas { get; private set; }
        public int busena { get; private set; }
        public string galiojimas { get; private set; }
        public bool pristatyta { get; private set; }
        public int kiekis { get; private set; }
        public double kaina { get; private set; }
        public int? nuolaida { get; private set; }
        public int? klientas { get; private set; }

        public Preke(int id, string rusis, string pavadinimas, string gamintojas, int busena, string galiojimas,
            bool pristatyta, int kiekis, double kaina, int? nuolaida, int? klientas)
        {
            ID = id;
            this.rusis = rusis;
            this.pavadinimas = pavadinimas;
            this.gamintojas = gamintojas;
            this.busena = busena;
            this.galiojimas = galiojimas;
            this.pristatyta = pristatyta;
            this.kiekis = kiekis;
            this.kaina = kaina;
            this.nuolaida = nuolaida;
            this.klientas = klientas;
        }

        public static List<Preke> Prekes()
        {
            List<Preke> list = new List<Preke>();
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "SELECT * FROM preke";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                int? nuolaida = null;
                int? klientas = null;
                if (!String.IsNullOrEmpty(dr["fk_Nuolaidaid_Nuolaida"].ToString()))
                {
                    nuolaida = int.Parse(dr["fk_Nuolaidaid_Nuolaida"].ToString());
                }
                if (!String.IsNullOrEmpty(dr["fk_Klientasid_Klientas"].ToString()))
                {
                    klientas = int.Parse(dr["fk_Klientasid_Klientas"].ToString());
                }
                Preke p = new Preke(
                    int.Parse(dr["id_Preke"].ToString()), 
                    dr["rusis"].ToString(), 
                    dr["pavadinimas"].ToString(),
                    dr["gamintojas"].ToString(), 
                    int.Parse(dr["busena"].ToString()), 
                    dr["galioja_iki"].ToString(),
                    bool.Parse(dr["pristatyta"].ToString()), 
                    int.Parse(dr["kiekis"].ToString()), 
                    double.Parse(dr["kaina"].ToString().Replace('.', ',')), nuolaida, klientas);
                list.Add(p);
            }
            db.connection.Close();
            return list;
        }

        public void Priskirti(Nuolaida n)
        {
            nuolaida = n.ID;
            Atnaujinti();
        }

        public void Atnaujinti()
        {
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "UPDATE `preke` SET `fk_Nuolaidaid_Nuolaida` = " + nuolaida + " WHERE `preke`.`id_Preke` = " + ID;
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            db.connection.Close();
        }
    }
}