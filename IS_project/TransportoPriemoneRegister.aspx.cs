﻿using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS_project
{
    public partial class TransportoPriemoneRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hl.NavigateUrl = "VairuotojaiRegister.aspx";
            hl2.NavigateUrl = "TransportoPriemoneRegister.aspx";
            hl3.NavigateUrl = "TransportoPriemoneAssign.aspx";
            hl4.NavigateUrl = "UzsakymaiAssign.aspx";
            if (!IsPostBack)
            {
                for (int i = 0, j = 2000; j < DateTime.Now.Year; i++, j++)
                    DropDownList1.Items.Add(j.ToString());
                DropDownList2.Items.Add("Sausio");
                DropDownList2.Items.Add("Vasario");
                DropDownList2.Items.Add("Kovo");
                DropDownList2.Items.Add("Balandžio");
                DropDownList2.Items.Add("Gegužės");
                DropDownList2.Items.Add("Birželio");
                DropDownList2.Items.Add("Liepos");
                DropDownList2.Items.Add("Rugpjūčio");
                DropDownList2.Items.Add("Rugsėjo");
                DropDownList2.Items.Add("Spalio");
                DropDownList2.Items.Add("Lapkričio");
                DropDownList2.Items.Add("Gruodžio");
                DropDownList2.Enabled = false;
                DropDownList3.Enabled = false;
                Button1.Enabled = false;
                if (String.IsNullOrEmpty((string)Session["error"]))
                {
                    Label1.Visible = false;
                }
                else
                {
                    Label1.Visible = true;
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = (string)Session["error"];
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Register();

        }

        protected void Register()
        {
            Label1.Visible = true;
            Label1.ForeColor = System.Drawing.Color.Red;
            string valstybinis_nr = TextBox1.Text;
            if (String.IsNullOrEmpty(valstybinis_nr))
            {
                Session["error"] = "Valstybinis numeris turi buti ivestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            Match m = Regex.Match(valstybinis_nr, @"[A-Z]{3}\d{3}");
            if (!m.Success)
            {
                Session["error"] = "Valstybinis numeris turi buti ivestas 3 didziosimis raidemis ir 3 skaiciais pvz: 'AAA555' ";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string marke = TextBox2.Text;
            if (String.IsNullOrEmpty(marke))
            {
                Session["error"] = "Transporto priemones marke turi buti ivesta";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string modelis = TextBox3.Text;
            if (String.IsNullOrEmpty(modelis))
            {
                Session["error"] = "Transporto priemones modelis turi buti ivestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string rida = TextBox6.Text;
            if (String.IsNullOrEmpty(rida))
            {
                Session["error"] = "Transporto priemones rida turi būti ivesta";
                Response.Redirect(Request.UrlReferrer.ToString());
            }   
            string kuras = RadioButtonList1.SelectedValue;
            string kebulas = RadioButtonList2.SelectedValue;
            string pavDez = RadioButtonList3.SelectedValue;
            string date = DropDownList1.SelectedValue + "-" + (DropDownList2.SelectedIndex + 1) + "-" + DropDownList3.SelectedValue;

            Ricardo_class.TransportoPriemone transportoPriemone = new Ricardo_class.TransportoPriemone
            (valstybinis_nr, marke, modelis, date, kuras, rida, kebulas, pavDez, 555.ToString(), ((Ricardo_class.LogistikosDarbuotojas)Session["LogistikosDarbuotojas"]).TabelioNr);
            transportoPriemone.Irasyti();
            Label1.ForeColor = System.Drawing.Color.Green;
            Session["error"] = "";
            Response.Redirect("LogistikosDarbuotojas.aspx");
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList2.Enabled = true;
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (DropDownList2.SelectedValue)
            {
                case "Sausio":
                case "Kovo":
                case "Gegužės":
                case "Liepos":
                case "Rugpjūčio":
                case "Spalio":
                case "Gruodžio":
                    for (int i = 0; i < 31; i++)
                        DropDownList3.Items.Add((i + 1).ToString());
                    break;
                case "Balandžio":
                case "Birželio":
                case "Rugsėjo":
                case "Lapkričio":
                    for (int i = 0; i < 30; i++)
                        DropDownList3.Items.Add((i + 1).ToString());
                    break;
                case "Vasario":
                    if (int.Parse(DropDownList1.SelectedValue) % 4 == 0)
                        for (int i = 0; i < 29; i++)
                            DropDownList3.Items.Add((i + 1).ToString());
                    else
                        for (int i = 0; i < 28; i++)
                            DropDownList3.Items.Add((i + 1).ToString());
                    break;
            }
            DropDownList3.Enabled = true;
        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1.Enabled = true;
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}