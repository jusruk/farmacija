using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IS_project.Edvino_class;
using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System.Data;

namespace IS_project.Ricardo_class
{
    public class LogistikosDarbuotojas
    {
        /// <summary>
        /// Logistikos darbuotojo vardas
        /// </summary>
        public string Vardas { get; private set; }
        /// <summary>
        /// Logistikos darbuotojo pavarde
        /// </summary>
        public string Pavarde {get; private set; }
        /// <summary>
        /// Logistikos darbuotojo tabelio numeris (unikalus)
        /// </summary>
        public string TabelioNr { get; private set; }
        /// <summary>
        /// Logistikos darbuotojo imone
        /// </summary>
        public string Imone { get; private set; }
        /// <summary>
        /// Logistikos darbuotojo telefono numeris
        /// </summary>
        public string TelNr { get; private set; }
        /// <summary>
        /// Logistikos darbuotojo elektroninis paštas
        /// </summary>
        public string ElPastas { get; private set; }
        
        /// <summary>
        /// Sukuriamas naujas logistikos imones darbuotojas
        /// </summary>
        /// <param name="vardas">Logistikos imones darbuotojo vardas</param>
        /// <param name="pavarde">Logistikos imones darbuotojo pavardė</param>
        /// <param name="tabNr">Logistikos imones darbuotojo tabelio numeris</param>
        /// <param name="imone">Logistikos imones darbuotojo imonė</param>
        /// <param name="tel">Logistikos imones darbuotojo telefono numeris</param>
        /// <param name="email">Logistikos imones darbuotojo elektroninis paštas</param>
        public LogistikosDarbuotojas(string vardas, string pavarde, string tabNr, string imone, string tel, string email)
        {
            Vardas = vardas;
            Pavarde = pavarde;
            TabelioNr = tabNr;
            Imone = imone;
            TelNr = tel;
            ElPastas = email;
        }

        public static LogistikosDarbuotojas Prisijungti(string uname, string psw)
        {
            DBConnect db = new DBConnect();
            db.connection.Open();

            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from logistikos_darbuotojas where tabelio_nr= '" + uname + "' and password='" + psw + "'";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            LogistikosDarbuotojas v = null;
            foreach (DataRow dr in dt.Rows)
            {
                v = new LogistikosDarbuotojas(
                    dr["vardas"].ToString(),
                    dr["pavarde"].ToString(),
                    dr["tabelio_nr"].ToString(),
                    dr["imone"].ToString(),
                    dr["tel_nr"].ToString(),
                    dr["el_pastas"].ToString());
            }
            db.connection.Close();
            return v;
        }
    }
}