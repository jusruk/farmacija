﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IS_project.Ricardo_class
{
    public class Tiekejas
    {
        /// <summary>
        /// Tiekejo pavadinimas
        /// </summary>
        public string Pavadinimas { get; private set; }
        /// <summary>
        /// Tiekejo adresas
        /// </summary>
        public string Adresas {get; private set; }
        /// <summary>
        /// Tiekėjo telefono numeris
        /// </summary>
        public string TelNr { get; private set; }
        /// <summary>
        /// Tiekėjo elektroninis paštas
        /// </summary>
        public string ElPastas { get; private set; }
        /// <summary>
        /// Tiekėjo miestas
        /// </summary>
        public string Miestas { get; private set; }
        
        /// <summary>
        /// Sukuriamas naujas tiekėjas
        /// </summary>
        /// <param name="pavadinimas">Tiekėjo pavadinimas</param>
        /// <param name="adresas">Tiekėjo adresas</param>
        /// <param name="tel">Tiekėjo telefono numeris</param>
        /// <param name="email">Tiekėjo elektroninis paštas</param>
        /// <param name="miestas">Tiekėjo miestas</param>
        public Tiekejas(string pavadinimas, string adresas, string tel, string email, string miestas)
        {
            Pavadinimas = pavadinimas;
            Adresas = adresas;
            TelNr = tel;
            ElPastas = email;
            Miestas = miestas;
        }
    }
}