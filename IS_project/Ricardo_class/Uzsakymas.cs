using IS_project.Igno_class;
using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IS_project.Edvino_class;

namespace IS_project.Ricardo_class
{
    public class Uzsakymas
    {
        /// <summary>
        /// Užsakymo id
        /// </summary>
        public string UzsakymoID { get; private set; }

        /// <summary>
        /// Vairuotojas, kuris pristato užsakymą
        /// </summary>
        public string Vairuotojas { get; private set; }
        /// <summary>
        /// Tiekėjas, kuris įvykdo užsakymą
        /// </summary>
        public string Tiekejas { get; private set; }
        /// <summary>
        /// Prekė, kuri įtraukiama į užsakymą
        /// </summary>
        public string Preke { get; private set; }
        /// <summary>
        /// Vaistinė, kuriai užsakymas pristatomas
        /// </summary>
        public string Vaistine { get; private set; }
        /// <summary>
        /// Vaistininkas, kuris sudaro užsakymą
        /// </summary>
        public string Vaistininkas { get; private set; }
        /// <summary>
        /// Sukuriamas užsakymas
        /// </summary>
        /// <param name="uzsakymoID">Užsakymo ID</param>
        /// <param name="Vairuotojas">Vairuotojas, kuris pristato užsakymą</param>
        /// <param name="Tiekejas">Tiekėjas, kuris įvykdo užsakymą</param>
        /// <param name="Vaistine">Vaistinė, kuriai užsakymas pristatomas</param>
        public Uzsakymas(string uzsakymoID, string Vairuotojas, string Tiekejas, string Preke, string Vaistine, string Vaistininkas)
        {
            UzsakymoID = uzsakymoID;
            this.Vairuotojas = Vairuotojas;
            this.Tiekejas = Tiekejas;
            this.Preke = Preke;
            this.Vaistine = Vaistine;
            this.Vaistininkas = Vaistininkas;
        }

        public static List<Uzsakymas> uzsakymas() 
        {
            List<Uzsakymas> list = new List<Uzsakymas>();
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "SELECT * FROM uzsakymas WHERE fk_Vairuotojaspazymejimo_nr = 555";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Uzsakymas u = new Uzsakymas(dr["uzsakymo_id"].ToString(), dr["fk_Tiekejasid_Tiekejas"].ToString(),
                    dr["fk_Vaistininkastabelio_nr"].ToString(), dr["fk_Vairuotojaspazymejimo_nr"].ToString(),
                    dr["fk_Vaistineid_Vaistine"].ToString(), dr["fk_Prekeid_Preke"].ToString());
                list.Add(u);
            }
            db.connection.Close();
            return list;
        }

        public void Priskirti(Vairuotojas v)
        {
            Vairuotojas = v.PazymejimoNr;
            Atnaujinti();
        }

        public void Atnaujinti()
        {
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "UPDATE `uzsakymas` SET `fk_Vairuotojaspazymejimo_nr` = " + Vairuotojas + " WHERE `uzsakymas`.`uzsakymo_id` = '" + UzsakymoID + "'";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            db.connection.Close();
        }
    }
}