﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IS_project.Edvino_class;
using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System.Data;

namespace IS_project.Ricardo_class
{
    public class Vairuotojas
    {
        /// <summary>
        /// Vairuotojo vardas
        /// </summary>
        public string Vardas { get; private set; }
        /// <summary>
        /// Vairuotojo pavardė
        /// </summary>
        public string Pavarde { get; private set; }
        /// <summary>
        /// Vairuotojo pažymėjimo numeris
        /// </summary>
        public string PazymejimoNr { get; private set; }
        /// <summary>
        /// Vairuotojo įmonė
        /// </summary>
        public string Imone { get; private set; }
        /// <summary>
        /// Vairuotojo telefono numeris
        /// </summary>
        public string TelNr { get; private set; }
        /// <summary>
        /// Vairuotojo elektroninis paštas
        /// </summary>
        public string ElPastas { get; private set; }
        /// <summary>
        /// Logistikos darbuotojas, kuris registruoja vairuotoją
        /// </summary>
        public string LogistikosDarbuotojas { get; private set; }
        /// <summary>
        /// Sukuriamas vairuotojas
        /// </summary>
        /// <param name="vardas">Vairuotojo vardas</param>
        /// <param name="pavarde">Vairuotojo pavardė</param>
        /// <param name="pazNr">Vairuotojo pažymėjimo numeris</param>
        /// <param name="imone">Vairuotojo įmonė</param>
        /// <param name="email">Vairuotojo elektroninis paštas</param>
        /// <param name="LogistikosDarbuotojas">Logistikos darbuotojas, kuris registruoja vairuotoją</param>
        public Vairuotojas(string vardas, string pavarde, string pazNr, string imone, string telNr, string email, string LogistikosDarbuotojas)
        {
            Vardas = vardas;
            Pavarde = pavarde;
            PazymejimoNr = pazNr;
            Imone = imone;
            TelNr = telNr;
            ElPastas = email;
            this.LogistikosDarbuotojas = LogistikosDarbuotojas;
        }

        public void Irasyti(string password)
        {
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "INSERT INTO vairuotojas" +
                " (`vardas`, `pavarde`,  `pazymejimo_nr`, `imone`, `tel_nr`, `el_pastas`, `password`, `fk_Logistikos_darbuotojastabelio_nr`) " +
                "VALUES ('" +
                Vardas + "', '" + Pavarde + "', '" +
                PazymejimoNr + "', '" + Imone + "', '" + TelNr +
                "', '" + ElPastas + "', '" + password + "', '" + LogistikosDarbuotojas + "')";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            db.connection.Close();
        }

        public static Vairuotojas Prisijungti(string uname, string psw)
        {
            DBConnect db = new DBConnect();
            db.connection.Open();

            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from vairuotojas where pazymejimo_nr= '" + uname + "' and password='" + psw + "'";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            Vairuotojas v = null;
            foreach (DataRow dr in dt.Rows)
            {
                v = new Vairuotojas(
                dr["vardas"].ToString(),
                dr["pavarde"].ToString(),
                dr["pazymejimo_nr"].ToString(),
                dr["imone"].ToString(),
                dr["tel_nr"].ToString(),
                dr["el_pastas"].ToString(),
                dr["fk_Logistikos_darbuotojastabelio_nr"].ToString());
            }
            db.connection.Close();
            return v;
        }

        public static List<Vairuotojas> vairuotojai()
        {
            List<Vairuotojas> list = new List<Vairuotojas>();
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "SELECT DISTINCT vairuotojas.* FROM vairuotojas, transporto_priemone WHERE " +
                "vairuotojas.pazymejimo_nr != transporto_priemone.fk_Vairuotojaspazymejimo_nr AND vairuotojas.pazymejimo_nr != 555";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Vairuotojas n = new Vairuotojas(dr["vardas"].ToString(), dr["pavarde"].ToString(),
                    dr["pazymejimo_nr"].ToString(), dr["imone"].ToString(), dr["tel_nr"].ToString(),
                    dr["el_pastas"].ToString(), dr["fk_Logistikos_darbuotojastabelio_nr"].ToString());
                list.Add(n);
            }
            db.connection.Close();
            return list;
        }

        public static List<Vairuotojas> vairuotojai2()
        {
            List<Vairuotojas> list = new List<Vairuotojas>();
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "SELECT DISTINCT vairuotojas.* FROM vairuotojas, uzsakymas " +
                "WHERE vairuotojas.pazymejimo_nr != uzsakymas.fk_Vairuotojaspazymejimo_nr AND vairuotojas.pazymejimo_nr != 555";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Vairuotojas n = new Vairuotojas(dr["vardas"].ToString(), dr["pavarde"].ToString(),
                    dr["pazymejimo_nr"].ToString(), dr["imone"].ToString(), dr["tel_nr"].ToString(),
                    dr["el_pastas"].ToString(), dr["fk_Logistikos_darbuotojastabelio_nr"].ToString());
                list.Add(n);
            }
            db.connection.Close();
            return list;
        }

    }
}