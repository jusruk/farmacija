﻿using IS_project.Igno_class;
using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IS_project.Ricardo_class
{
    public class TransportoPriemone
    {
        /// <summary>
        /// Transporto priemonės valstybinis numeris
        /// </summary>
        public string ValNr { get; private set; }
        /// <summary>
        /// Transporto priemonės markė
        /// </summary>
        public string Marke {get; private set; }
        /// <summary>
        /// Transporto priemonės modelis
        /// </summary>
        public string Modelis { get; private set; }
        /// <summary>
        /// Transporto priemonės pagaminimo metai
        /// </summary>
        public string PagMetai { get; private set; }
        /// <summary>
        /// Transporto priemonės kuro tipas
        /// </summary>
        public string Kuras { get; private set; }
        /// <summary>
        /// Transporto priemonės rida
        /// </summary>
        public string Rida { get; private set; }
        /// <summary>
        /// Transporto priemonės kėbulo tipas
        /// </summary>
        public string Kebulas { get; private set; }
        /// <summary>
        /// Transporto priemonės pavarų dėžės tipas
        /// </summary>
        public string PavDeze { get; private set; }
        
        /// <summary>
        /// Vairuotojas, kuris vairuoja transporto priemonę
        /// </summary>
        public string Vairuotojas { get; private set; }
        /// <summary>
        /// Logistikos darbuotojas, kuris registruoja transporto priemonę
        /// </summary>
        public string LogistikosDarbuotojas { get; private set; }
        
        /// <summary>
        /// Sukuriama transporto priemonė
        /// </summary>
        /// <param name="valNr">Transporto priemonės valstybinis numeris</param>
        /// <param name="marke">Transporto priemonės markė</param>
        /// <param name="modelis">Transporto priemonės modelis</param>
        /// <param name="pagMetai">Transporto priemonės pagaminimo metai</param>
        /// <param name="kuras">Transporto priemonės kuro tipas</param>
        /// <param name="rida">Transporto priemonės rida</param>
        /// <param name="kebulas">Transporto priemonės kėbulo tipas</param>
        /// <param name="pavDeze">Transporto priemonės pavarų dėžės tipas</param>
        public TransportoPriemone(string valNr, string marke, string modelis, string pagMetai, string kuras, string rida, string kebulas, string pavDeze, string Vairuotojas, string LogistikosDarbuotojas)
        {
            ValNr = valNr;
            Marke = marke;
            Modelis = modelis;
            PagMetai = pagMetai;
            Kuras = kuras;
            Rida = rida;
            Kebulas = kebulas;
            PavDeze = pavDeze;
            this.Vairuotojas = Vairuotojas;
            this.LogistikosDarbuotojas = LogistikosDarbuotojas;
        }

        public void Irasyti()
        {
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "INSERT INTO transporto_priemone" +
                " (`valstybinis_nr`, `marke`, `modelis`, `pagaminimo_metai`, `kuras`, `rida`, `kebulas`, `pavaru_deze`, `fk_Vairuotojaspazymejimo_nr`, `fk_Logistikos_darbuotojastabelio_nr`) " +
                "VALUES ('" +
                ValNr + "', '" + Marke + "', '" +
                Modelis + "', '" + PagMetai + "', '" + Kuras +
                "', '" + Rida + "', '" + Kebulas + "', '" + PavDeze + "', '" + Vairuotojas + "', '" + LogistikosDarbuotojas +"')";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            db.connection.Close();
        }

        public static List<TransportoPriemone> TrPriemones()
        {
            List<TransportoPriemone> list = new List<TransportoPriemone>();
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "SELECT * FROM transporto_priemone WHERE fk_Vairuotojaspazymejimo_nr = 555";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                TransportoPriemone n = new TransportoPriemone(dr["valstybinis_nr"].ToString(), dr["marke"].ToString(),
                    dr["modelis"].ToString(), dr["pagaminimo_metai"].ToString(), dr["kuras"].ToString(),
                    dr["rida"].ToString(), dr["kebulas"].ToString(), dr["pavaru_deze"].ToString(),
                    dr["fk_Vairuotojaspazymejimo_nr"].ToString(), dr["fk_Logistikos_darbuotojastabelio_nr"].ToString());
                list.Add(n);
            }
            db.connection.Close();
            return list;
        }

        public void Priskirti(Vairuotojas v)
        {
            Vairuotojas = v.PazymejimoNr;
            Atnaujinti();
        }

        public void Atnaujinti()
        {
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "UPDATE `transporto_priemone` SET `fk_Vairuotojaspazymejimo_nr` = " + Vairuotojas + " WHERE `transporto_priemone`.`valstybinis_nr` = '" + ValNr +"'";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            db.connection.Close();
        }
    }
}
