﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pagrindinis.aspx.cs" Inherits="IS_project.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
    <title>Login</title>
    <style type="text/css">
        body {
    background-image: url('images/background.jpg');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-clip:border-box;
    background-size:100% 100%;
    background-position: center center;
}
        
        .div {
            margin-top: 150px;
        }

        * {
            padding: auto;
            margin-left: auto;
            margin-right: auto;
            text-align: center;
        }

        .container {
            margin-top: 210px;
           margin-bottom:50px;
            padding: auto;
            width: 40%;
            border-radius: 25px;
            background-color:rgba(128, 128, 128,0.4);
            position:center center;
        }

        .btn-group-lg > .btn, .btn-lg {
            padding: 10px 16px;
            font-size: 18px;
            line-height: 1.3333333;
            border-radius: 6px;
            margin-bottom: 20px;
        }
        .DropDownList1 {
            float:right;
            width:10%;
        }
        .dropDown {
            width:200px;
            float:right;
            margin-right:40px;
        }
    </style>
</head>
<body>


    <body>
        <form id="form2" runat="server">
     <h2 class ="text-left" style="padding-left:40px;">FARMACIJOS INFORMACINĖ SISTEMA&nbsp;&nbsp;
         <div class="dropDown">
         <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">
             <asp:ListItem>Prisijungti</asp:ListItem>
             <asp:ListItem>Vaistininkas</asp:ListItem>
             <asp:ListItem>Vadybininkas</asp:ListItem>
             <asp:ListItem Value="Logistikos_darbuotojas">Logistikos darbuotojas</asp:ListItem>
             <asp:ListItem>Vairuotojas</asp:ListItem>
             <asp:ListItem>Klientas</asp:ListItem>
         </asp:DropDownList>
             </div>


            </h2>
            <div>
                <div class="container">
                    
                      
                        <h1>SVEIKI ATVYKĘ!</h1>
                    prašome prisijunti prie sistemos
                      
                </div>
            </div>
        </form>
    </body>
</body>
</html>
