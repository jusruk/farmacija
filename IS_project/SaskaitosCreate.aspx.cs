﻿using IS_project.Shared_class;
using IS_project.Igno_class;
using IS_project.Justino_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS_project
{
    public partial class SaskaitosCreate : Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            hl.NavigateUrl = "VaistininkaiRegister.aspx";
            hl1.NavigateUrl = "SaskaitosCreate.aspx";
            hl2.NavigateUrl = "Nuolaidos.aspx";
            hl3.NavigateUrl = "SaskaitosAssign.aspx";
            hl4.NavigateUrl = "AtaskaitaNeapmoketos.aspx";
            hl5.NavigateUrl = "AtaskaitaPermoketos.aspx";
            if (((Justino_class.Vaistininkas)Session["vaistininkas"]) != null)
            {
                if (!IsPostBack)
                {
                    List<PrekiuKrepselis> pk = PrekiuKrepselis.Krepseliai();
                    if (pk.Count != 0)
                    {
                        if (String.IsNullOrEmpty((string)Session["error"]))
                        {
                            Label1.Visible = false;
                        }
                        else
                        {
                            Label1.Visible = true;
                            Label1.ForeColor = System.Drawing.Color.Red;
                            Label1.Text = (string)Session["error"];
                        }
                        DropDownList1.Items.Add("Pasirinkite prekių krepšelį");
                        foreach (var p in pk)
                            DropDownList1.Items.Add(p.ID);
                    }
                }
                else if (PrekiuKrepselis.Krepseliai().Count == 0)
                {
                    Label1.Visible = true;
                    Button1.Enabled = false;
                }
                else
                {
                    Label1.Visible = false;
                }
            }
            else Response.Redirect(Request.UrlReferrer.ToString());
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Create();
        }

        protected void Create()
        {
            List<PrekiuKrepselis> pk = PrekiuKrepselis.Krepseliai();
            string id = TextBox1.Text;
            if (String.IsNullOrEmpty(id))
            {
                Session["error"] = "ID turi būti įvestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            int tipas = RadioButtonList1.SelectedIndex;
            string dateNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            int apmoketa = RadioButtonList2.SelectedIndex;
            string dateApmokejimas;
            if (apmoketa == 0)
            {
                dateApmokejimas = "";
            }
            else
            {
                dateApmokejimas = Calendar2.SelectedDate.ToString("yyyy-MM-dd HH:mm:ss");
            }
            string suma = pk[DropDownList1.SelectedIndex - 1].suma.ToString().Replace(',', '.');
            string busena = "nesusieta";
            string pk_id = "";
            if (DropDownList1.SelectedIndex != 0)
            {
                pk_id = pk[DropDownList1.SelectedIndex - 1].ID;
            }
            else
            {
                Session["error"] = DropDownList1.SelectedValue;
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            int kiekis = pk[DropDownList1.SelectedIndex - 1].kiekis;
            Saskaita sas = new Saskaita(int.Parse(id), suma, tipas.ToString(), dateNow, apmoketa, dateApmokejimas, busena, pk_id);
            sas.Irasyti();
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}