﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IS_project.Edvino_class;
using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System.Data;

namespace IS_project.Justino_class
{
    /// <summary>
    /// Vaistininko duomenų klasė
    /// </summary>
    public class Vaistininkas
    {
        /// <summary>
        /// Vaisitininko tabelio numeris (unikalus)
        /// </summary>
        public string TabelioNr { get; private set; }
        /// <summary>
        /// Vaistininko vardas
        /// </summary>
        public string Vardas { get; private set; }
        /// <summary>
        /// Vaistininko pavardė
        /// </summary>
        public string Pavarde { get; private set; }
        /// <summary>
        /// Vaistininko banko sąskaita
        /// </summary>
        public string BankoSas { get; private set; }
        /// <summary>
        /// Vaistininko telefono numeris
        /// </summary>
        public string TelNr { get; private set; }
        /// <summary>
        /// Vaistininko elektroninis paštas
        /// </summary>
        public string ElPastas { get; private set; }
        /// <summary>
        /// Vaistininko lytis
        /// </summary>
        public string Lytis { get; private set; }
        /// <summary>
        /// Vaistininko stažas
        /// </summary>
        public int Stazas { get; private set; }
        /// <summary>
        /// Vaistininko gimimo metai
        /// </summary>
        public string GimData { get; private set; }
        /// <summary>
        /// Vaistinė, kurioje dirba vaistininkas
        /// </summary>
        public int Vaistine { get; private set; }
        
        /// <summary>
        /// Sukuriama nauja vaistinė
        /// </summary>
        /// <param name="tabNr">Vaistininko tabelio numeris</param>
        /// <param name="vardas">Vaistininko vardas</param>
        /// <param name="pavarde">Vaistininko pavardė</param>
        /// <param name="bank">Vaistininko banko sąskaita</param>
        /// <param name="tel">Vaistininko telefono numeris</param>
        /// <param name="email">Vaistininko elektroninis paštas</param>
        /// <param name="lytis">Vaistininko lytis</param>
        /// <param name="stazas">Vaistininko stažas</param>
        /// <param name="metai">Vaistininko gimimo metai</param>
        /// <param name="vaistine">Vaistinė, kurioje dirba vaistininkas</param>
        public Vaistininkas(string tabNr, string vardas, string pavarde, string bank, string tel, string email, string lytis, int stazas, string metai, int vaistine)
        {
            TabelioNr = tabNr;
            Vardas = vardas;
            Pavarde = pavarde;
            BankoSas = bank;
            TelNr = tel;
            ElPastas = email;
            Lytis = lytis;
            Stazas = stazas;
            GimData = metai;
            Vaistine = vaistine;
        }

        public void Irasyti(string password)
        {
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "INSERT INTO vaistininkas" +
                " (`tabelio_nr`, `password`, `vardas`, `pavarde`, `banko_sas`, `tel_nr`, `el_pastas`, `lytis`, `stazas`, `gimimo_data`, `fk_Vaistineid_Vaistine`) " +
                "VALUES ('" +
                TabelioNr + "', '" + password + "', '" +
                Vardas+ "', '" + Pavarde+ "', '" + BankoSas+
                "', '" + TelNr+ "', '" + ElPastas+ "', '" + Lytis+ "', '" + Stazas+ "', '" +
                GimData + "', '" + Vaistine + "')";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            db.connection.Close();
        }

        public static Vaistininkas Prisijungti(string uname, string psw)
        {
            DBConnect db = new DBConnect();
            db.connection.Open();

            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from vaistininkas where tabelio_nr= '" + uname + "' and password='" + psw + "'";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            Vaistininkas v = null;
            foreach (DataRow dr in dt.Rows)
            {
                v = new Vaistininkas(
                    dr["tabelio_nr"].ToString(),
                    dr["vardas"].ToString(),
                    dr["pavarde"].ToString(),
                    dr["banko_sas"].ToString(),
                    dr["tel_nr"].ToString(),
                    dr["el_pastas"].ToString(),
                    dr["lytis"].ToString(),
                    int.Parse(dr["stazas"].ToString()),
                    dr["gimimo_data"].ToString(),
                    int.Parse(dr["fk_Vaistineid_Vaistine"].ToString()));
            }
            db.connection.Close();
            return v;
        }
    }
}