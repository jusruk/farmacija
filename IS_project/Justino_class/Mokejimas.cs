﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IS_project.Igno_class;
using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System.Data;

namespace IS_project.Justino_class
{
    /// <summary>
    /// Mokėjimo duomenų klasė
    /// </summary>
    public class Mokejimas
    {
        /// <summary>
        /// Mokėjimo ID (unikalus)
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Mokėjimo data
        /// </summary>
        public string Data { get; private set; }
        /// <summary>
        /// Mokėjimo suma
        /// </summary>
        public double Suma { get; private set; }
        /// <summary>
        /// Mokėjimo tipas (grynais, pavedimu)
        /// </summary>
        public int Tipas { get; private set; }
        /// <summary>
        /// Mokėjimo būsena (pabaigta, nepabaigta)
        /// </summary>
        public string Busena { get; private set; }
        /// <summary>
        /// Mokėjimą padarusio kliento duomenys
        /// </summary>
        public int? Klientas{ get; private set; }
        /// <summary>
        /// Su mokėjimu susieta sąskaita
        /// </summary>
        public int? Saskaita { get; private set; }

        /// <summary>
        /// Sukuriamas mokėjimo objektas
        /// </summary>
        /// <param name="id">Mokėjimo ID</param>
        /// <param name="suma">Mokėjimo suma</param>
        /// <param name="tipas">Mokėjimo tipas</param>
        /// <param name="busena">Mokėjimo būsena</param>
        /// <param name="klientas">Mokėjimą padaręs klientas</param>
        /// <param name="sas">Su mokėjimu susieta sąskaitas</param>
        public Mokejimas(int id, string data, double suma, int tipas, string busena, int? klientas, int? sas)
        {
            ID = id;
            Data = data;
            Suma = suma;
            Tipas = tipas;
            Busena = busena;
            Klientas = klientas;
            Saskaita = sas;
        }

        public void Atnaujinti (int saskaita)
        {
            Saskaita = saskaita;
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "UPDATE `mokejimas` SET `fk_Saskaitaid` = " + Saskaita + " WHERE `mokejimas`.`id_Mokejimas` = " + ID;
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            db.connection.Close();
        }

        public static List<Mokejimas> NesusietiMokejimai()
        {
            List<Mokejimas> list = new List<Mokejimas>();
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "SELECT * FROM mokejimas WHERE mokejimas.fk_Saskaitaid IS NULL";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                int? saskaita = null;
                int? klientas = null;
                if (!String.IsNullOrEmpty(dr["fk_Saskaitaid"].ToString()))
                {
                    saskaita = int.Parse(dr["fk_Saskaitaid"].ToString());
                }
                if (!String.IsNullOrEmpty(dr["fk_Klientasid_Klientas"].ToString()))
                {
                    klientas = int.Parse(dr["fk_Klientasid_Klientas"].ToString());
                }
                Mokejimas m = new Mokejimas(int.Parse(dr["id_Mokejimas"].ToString()), dr["data"].ToString(),double.Parse(dr["suma"].ToString().Replace('.', ',')), 
                    int.Parse(dr["tipas"].ToString()), dr["busena"].ToString(), klientas, saskaita);
                list.Add(m);
            }
            db.connection.Close();
            return list;
        }
    }
}