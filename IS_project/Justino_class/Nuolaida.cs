﻿using IS_project.Igno_class;
using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IS_project.Justino_class
{
    /// <summary>
    /// Nuolaidos duomenų klasė
    /// </summary>
    public class Nuolaida
    {
        /// <summary>
        /// Nuolaidos ID (unikalus)
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Nuolaidos tipas
        /// </summary>
        public string Tipas { get; private set; }
        /// <summary>
        /// Nuolaidos skaičiavimo būdas
        /// </summary>
        public int SkaicBudas { get; private set; }
        /// <summary>
        /// Nuolaidos dydis
        /// </summary>
        public string Dydis { get; private set; }
        /// <summary>
        /// Nuolaidos pradžia
        /// </summary>
        public string Pradzia { get; private set; }
        /// <summary>
        /// Nuolaidos pabaiga
        /// </summary>
        public string Pabaiga { get; private set; }
        /// <summary>
        /// Nuolaidos būsena
        /// </summary>
        public string Busena { get; private set; }

        /// <summary>
        /// Sukuriama nauja nuolaida
        /// </summary>
        /// <param name="id">Nuolaidos ID</param>
        /// <param name="tipas">Nuolaidos tipas</param>
        /// <param name="budas">Nuolaidos skaičiavimo būdas</param>
        /// <param name="dydis">Nuolaidos dydis</param>
        /// <param name="pradzia">Nuolaidos pradžios data</param>
        /// <param name="pabaiga">Nuolaidos pabaigos data</param>
        /// <param name="busena">Nuolaidos būsena</param>
        public Nuolaida(int id, string tipas, int budas, string dydis, string pradzia, string pabaiga, string busena)
        {
            ID = id;
            Tipas = tipas;
            SkaicBudas = budas;
            Dydis = dydis;
            Pradzia = pradzia;
            Pabaiga = pabaiga;
            Busena = busena;
        }

        public static List<Nuolaida> Nuolaidos()
        {
            List<Nuolaida> list = new List<Nuolaida>();
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "SELECT * FROM nuolaida";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Nuolaida n = new Nuolaida(int.Parse(dr["id_nuolaida"].ToString()), dr["tipas"].ToString(),
                    int.Parse(dr["skaic_budas"].ToString()), dr["dydis"].ToString(),
                    dr["pradzios_data"].ToString(), dr["pabaigos_data"].ToString(), dr["busena"].ToString());
                list.Add(n);
            }
            db.connection.Close();
            return list;
        }
    }
}