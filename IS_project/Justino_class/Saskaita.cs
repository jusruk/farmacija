﻿using IS_project.Shared_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IS_project.Justino_class
{
    /// <summary>
    /// Sąskaitos duomenų klasė
    /// </summary>
    public class Saskaita
    {
        /// <summary>
        /// Sąskaitos ID (unikalus)
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Sąskaitos suma
        /// </summary>
        public string Suma { get; private set; }
        /// <summary>
        /// Sąskaitos tipas
        /// </summary>
        public string Tipas { get; private set; }
        /// <summary>
        /// Sąskaitos sudarymo data
        /// </summary>
        public string SudarymoData { get; private set; }
        /// <summary>
        /// Ar sąskaita apmoketa
        /// </summary>
        public int Apmoketa { get; private set; }
        /// <summary>
        /// Saskaitos apmokejimo data
        /// </summary>
        public string ApmokejimoData { get; private set; }
        /// <summary>
        /// Sąskaitos būsena
        /// </summary>
        public string Busena { get; private set; }
        /// <summary>
        /// Prekių krepšelis, iš kurio sudaryta sąskaita
        /// </summary>
        public string Krepselis { get; private set; }

        /// <summary>
        /// Sukuriama nauja sąskaita
        /// </summary>
        /// <param name="id">Sąskaitos ID</param>
        /// <param name="suma">Sąskaitos suma</param>
        /// <param name="tipas">Sąskaitos tipas</param>
        /// <param name="sudaryta">Sąskaitos sudarymo data</param>
        /// <param name="apmoketa">Ar sąskaita apmokėta</param>
        /// <param name="apmokejimas">Sąskaitos apmokėjimo data</param>
        /// <param name="busena">Sąskaitos būsena</param>
        /// <param name="krepselis">Krepšelis, iš kurio sudaryta sąskaita</param>
        public Saskaita(int id, string suma, string tipas, string sudaryta, int apmoketa, string apmokejimas, string busena, string krepselis)
        {
            ID = id;
            Suma = suma;
            Tipas = tipas;
            SudarymoData = sudaryta;
            Apmoketa = apmoketa;
            ApmokejimoData = apmokejimas;
            Busena = busena;
            Krepselis = krepselis;
        }

        public void Irasyti()
        {
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "INSERT INTO `saskaita`(`id`, `suma`, `tipas`, `sudarymo_data`, `apmoketa`, `apmokejimo_data`, `busena`, `fk_Prekiu_krepselisid`) " +
                "VALUES ('" + ID + "', '" + Suma + "', '" +
                Tipas + "', '" + SudarymoData + "', '" + Apmoketa +
                "', '" + ApmokejimoData + "', '" + Busena + "', '" + Krepselis + "')";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            db.connection.Close();
        }

        public static List<Saskaita> NesusietosSaskaitos()
        {
            List<Saskaita> list = new List<Saskaita>();
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "SELECT * FROM saskaita WHERE apmoketa = 0";
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                int ap = -1;
                bool apmoketa = bool.Parse(dr["apmoketa"].ToString());
                if (apmoketa) ap = 1; else ap = 0;
                Saskaita s = new Saskaita(int.Parse(dr["id"].ToString()), dr["suma"].ToString().Replace(',', '.'),
                    dr["tipas"].ToString(), dr["sudarymo_data"].ToString(), ap,
                    dr["apmokejimo_data"].ToString(), dr["busena"].ToString(), dr["fk_Prekiu_krepselisid"].ToString());
                list.Add(s);
            }
            db.connection.Close();
            return list;
        }

        public void Susieti(Mokejimas m)
        {
            Apmoketa = 1;
            ApmokejimoData = m.Data;
            Busena = "susieta";
            Atnaujinti();
            m.Atnaujinti(ID);
        }

        public void Atnaujinti()
        {
            DBConnect db = new DBConnect();
            db.connection.Open();
            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string query = "UPDATE `saskaita` SET `apmoketa` = " + Apmoketa + ", `busena` = '" + Busena + "', `apmokejimo_data` = '" + ApmokejimoData.Substring(0,10) + "' WHERE `saskaita`.`id` = " + ID;
            cmd.CommandText = query;
            cmd.ExecuteNonQuery();
            db.connection.Close();
        }
    }
}