﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IS_project.Shared_class;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;

namespace IS_project
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue == "Vadybininkas")
            {
                Response.Redirect("VadybininkasLogin.aspx");
            }
            if (DropDownList1.SelectedValue == "Vaistininkas")
            {
                Response.Redirect("VaistininkaiLogin.aspx");
            }
            if (DropDownList1.SelectedValue == "Vairuotojas")
            {
                Response.Redirect("VairuotojaiLogin.aspx");
            }
            if (DropDownList1.SelectedValue == "Logistikos_darbuotojas")
            {
                Response.Redirect("LogistikosDarbuotojasLogin.aspx");
            }
            if (DropDownList1.SelectedValue == "Klientas")
            {
                Response.Redirect("KlientasLogin.aspx");
            }

        }

    }
}