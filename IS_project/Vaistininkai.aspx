﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Vaistininkai.aspx.cs" Inherits="IS_project.Vaistininkas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>

    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="Vaistininkai.aspx">Farmacijos sistema</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="Vaistininkai.aspx">Home</a></li>
                    <li><asp:HyperLink id="hl" runat="server" Text="Registruoti vaistininką"/></li>
                    <li><asp:HyperLink id="hl1" runat="server" Text="Sudaryti sąskaitą"/></li>
                    <li><asp:HyperLink id="hl2" runat="server" Text="Teikti nuolaidą prekei"/></li>
                    <li><asp:HyperLink id="hl3" runat="server" Text="Susieti sąskaitą su mokėjimu"/></li>
                    <li><asp:HyperLink ID="hl4" runat="server" Text="Neapmokėtų sąskaitų ataskaita"/></li>
                    <li><asp:HyperLink ID="hl5" runat="server" Text="Permokėtų sąskaitų ataskaita"/></li>
                <li>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <h2><center>Profilio informacija</h2>
            <p>
                <center>vardas:<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></center>
            </p>
            </center>
            <br />
            <br />

            <p>
               <h3><center>Prekių informacija</center></h3> </p>
            <p>
              <center><asp:GridView ID="GridView1" runat="server">
                </asp:GridView></center>  
            </p>
            <p><br />
                <br />

                 <h3><center> Sąskaitų informacija</center>
               </p>
            <p>
               <center><asp:GridView ID="GridView2" runat="server">
                </asp:GridView></center> 
            </p>
            <p>
                <br />
                <br />
                 <h3><center> Mokėjimų informacija</center>
               </p>
            <p>
                 <center><asp:GridView ID="GridView3" runat="server">
                </asp:GridView></center>
            </p>
        </div>
    </form>
</body>
</html>
