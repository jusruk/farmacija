﻿using IS_project.Justino_class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS_project
{
    public partial class SaskaitosAssign : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hl.NavigateUrl = "VaistininkaiRegister.aspx";
            hl1.NavigateUrl = "SaskaitosCreate.aspx";
            hl2.NavigateUrl = "Nuolaidos.aspx";
            hl3.NavigateUrl = "SaskaitosAssign.aspx";
            hl4.NavigateUrl = "AtaskaitaNeapmoketos.aspx";
            hl5.NavigateUrl = "AtaskaitaPermoketos.aspx";
            if (((Justino_class.Vaistininkas)Session["vaistininkas"]) != null)
            {
                if (!IsPostBack)
                {
                    Label2.Visible = false;
                    List<Saskaita> saskaitos = Saskaita.NesusietosSaskaitos();
                    if (saskaitos.Count != 0)
                    {
                        if (String.IsNullOrEmpty((string)Session["error"]))
                        {
                            Label1.Visible = false;
                        }
                        else
                        {
                            Label1.Visible = true;
                            Label1.ForeColor = System.Drawing.Color.Red;
                            Label1.Text = (string)Session["error"];
                        }
                        DropDownList1.Items.Add("Pasirinkite sąskaitą");
                        foreach (var s in saskaitos)
                            DropDownList1.Items.Add(s.ID.ToString());
                    }
                    else
                    {
                        Label2.Visible = true;
                        Label3.Visible = false;
                        Label4.Visible = false;
                        Button1.Visible = false;
                        DropDownList1.Visible = false;
                        DropDownList2.Visible = false;
                        return;
                    }
                    List<Mokejimas> mokejimai = Mokejimas.NesusietiMokejimai();
                    if (mokejimai.Count != 0)
                    {
                        if (String.IsNullOrEmpty((string)Session["error"]))
                        {
                            Label1.Visible = false;
                        }
                        else
                        {
                            Label1.Visible = true;
                            Label1.ForeColor = System.Drawing.Color.Red;
                            Label1.Text = (string)Session["error"];
                        }
                        DropDownList2.Items.Add("Pasirinkite mokėjimą");
                        foreach (var m in mokejimai)
                            DropDownList2.Items.Add(m.ID.ToString());
                    }
                }
                else if (Saskaita.NesusietosSaskaitos().Count == 0 || Mokejimas.NesusietiMokejimai().Count == 0)
                {
                    Label1.Visible = true;
                    Button1.Enabled = false;
                }
                else
                {
                    Label1.Visible = false;
                }
            }
            else if (Request.UrlReferrer == null)
            {
                Response.Redirect("Pagrindinis.aspx");
            }
            else
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<Saskaita> saskaitos = Saskaita.NesusietosSaskaitos();
            List<Mokejimas> mokejimai = Mokejimas.NesusietiMokejimai();
            if (DropDownList1.SelectedIndex != 0 && DropDownList2.SelectedIndex != 0)
            {
                Saskaita s = saskaitos[DropDownList1.SelectedIndex - 1];
                Mokejimas m = mokejimai[DropDownList2.SelectedIndex - 1];
                s.Susieti(m);
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}