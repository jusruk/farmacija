﻿using IS_project.Shared_class;
using IS_project.Edvino_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace IS_project
{
    public partial class PrSrautai : System.Web.UI.Page
    {
        DBConnect db = new DBConnect();
        int filtras;
        protected void Page_Load(object sender, EventArgs e)
        {
            hl.NavigateUrl = "Inventorius.aspx";
            hl2.NavigateUrl = "Patalpos.aspx";
            hl3.NavigateUrl = "Tvarkarastis.aspx";
            hl4.NavigateUrl = "PrSrautai.aspx";
            if (!IsPostBack)
            {
                DropDownList2.Items.Insert(0, new ListItem("Visos prekės", "0"));
                DropDownList2.Items.Insert(1, new ListItem("Likusios prekės", "1"));
                DropDownList2.Items.Insert(2, new ListItem("Parduotos prekės", "2"));
                DropDownList2.Items.Insert(3, new ListItem("Akcijinės prekės", "3"));
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            db.connection.Open();
            filtras = int.Parse(DropDownList2.SelectedItem.Value);
            filter(filtras);
            db.connection.Close();
        }
        protected void filter(int filtras)
        {
            if (filtras == 0)
            {
                string query = "select pavadinimas,gamintojas,kiekis,kaina,busena from preke";
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                cmd.CommandType = CommandType.Text;

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    GridView1.DataSource = dr;
                    GridView1.DataBind();
                }
                
              
                MySqlCommand cmdd = db.connection.CreateCommand();
                cmdd.CommandType = CommandType.Text;
                cmdd.CommandText = "select sum(kaina) from preke where kaina !=0";
                cmdd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(cmdd);
                da.Fill(dt);
                Vadybininkas v = null;
                foreach (DataRow dr in dt.Rows)
                {
                    Label1.Text = "Bendra prekių suma: "+dr[0].ToString()+" EUR";
                }
                db.connection.Close();


           
       

            }
            if (filtras == 1)
            {
                string query = "select pavadinimas,gamintojas,kiekis,kaina,busena from preke where busena = 1";
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                cmd.CommandType = CommandType.Text;

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    GridView1.DataSource = dr;
                    GridView1.DataBind();
                }


                MySqlCommand cmdd = db.connection.CreateCommand();
                cmdd.CommandType = CommandType.Text;
                cmdd.CommandText = "select sum(kaina) from preke where kaina !=0 and busena = 1";
                cmdd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(cmdd);
                da.Fill(dt);
                Vadybininkas v = null;
                foreach (DataRow dr in dt.Rows)
                {
                    Label1.Text = "Bendra prekių suma: " + dr[0].ToString() + " EUR";
                }
                db.connection.Close();
            }
            if (filtras == 2)
            {
                string query = "select pavadinimas,gamintojas,kiekis,kaina,busena from preke where busena = 0";
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                cmd.CommandType = CommandType.Text;
                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    
                    GridView1.DataSource = dr;

                    GridView1.DataBind();
                }
                MySqlCommand cmdd = db.connection.CreateCommand();
                cmdd.CommandType = CommandType.Text;
                cmdd.CommandText = "select sum(kaina) from preke where kaina !=0 and busena = 0";
                cmdd.ExecuteNonQuery();
                DataTable dt = new DataTable();
                MySqlDataAdapter da = new MySqlDataAdapter(cmdd);
                da.Fill(dt);
                Vadybininkas v = null;
                foreach (DataRow dr in dt.Rows)
                {
                    Label1.Text = "Bendra prekių suma: " + dr[0].ToString() + " EUR";
                }
                db.connection.Close();
            }
            if (filtras == 3)
            {
                string query = "select pavadinimas,gamintojas,kiekis,kaina,preke.busena,dydis, tipas from preke,nuolaida where preke.fk_Nuolaidaid_Nuolaida = nuolaida.id_nuolaida";
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                cmd.CommandType = CommandType.Text;
                using (MySqlDataReader dr = cmd.ExecuteReader())
                {

                    GridView1.DataSource = dr;
                    GridView1.DataBind();
                }

                Label1.Text = "";
            }

        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Pavadinimas";
                e.Row.Cells[1].Text = "Gamintojas";
                e.Row.Cells[2].Text = "Kiekis";
                e.Row.Cells[3].Text = "Kaina";
                e.Row.Cells[4].Text = "Būsena";

            }
            if(e.Row.Cells[4].Text == "0")
            {
                e.Row.Cells[4].Text = "nėra";
            }else if(e.Row.Cells[4].Text == "1")
            {
                e.Row.Cells[4].Text = "yra";
            }
            if(e.Row.Cells.Count >6) { 
            if(e.Row.Cells[6].Text == "0")
            {
                e.Row.Cells[6].Text = "%";
            }
            else if (e.Row.Cells[6].Text == "1")
            {
                e.Row.Cells[6].Text = "EUR";
            }
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}