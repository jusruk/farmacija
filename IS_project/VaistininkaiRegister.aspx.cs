﻿using IS_project.Shared_class;
using IS_project.Justino_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace IS_project
{
    public partial class VaistininkasRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hl.NavigateUrl = "VaistininkaiRegister.aspx";
            hl1.NavigateUrl = "SaskaitosCreate.aspx";
            hl2.NavigateUrl = "Nuolaidos.aspx";
            hl3.NavigateUrl = "SaskaitosAssign.aspx";
            hl4.NavigateUrl = "AtaskaitaNeapmoketos.aspx";
            hl5.NavigateUrl = "AtaskaitaPermoketos.aspx";
            if (((Justino_class.Vaistininkas)Session["vaistininkas"]) != null)
            {
                if (!IsPostBack)
                {
                    DropDownList1.Items.Add("Pasirinkite metus");
                    for (int j = 1950; j < DateTime.Now.Year; j++)
                        DropDownList1.Items.Add(j.ToString());
                    DropDownList2.Items.Add("Pasirinkite mėnęsį");
                    DropDownList2.Items.Add("Sausio");
                    DropDownList2.Items.Add("Vasario");
                    DropDownList2.Items.Add("Kovo");
                    DropDownList2.Items.Add("Balandžio");
                    DropDownList2.Items.Add("Gegužės");
                    DropDownList2.Items.Add("Birželio");
                    DropDownList2.Items.Add("Liepos");
                    DropDownList2.Items.Add("Rugpjūčio");
                    DropDownList2.Items.Add("Rugsėjo");
                    DropDownList2.Items.Add("Spalio");
                    DropDownList2.Items.Add("Lapkričio");
                    DropDownList2.Items.Add("Gruodžio");
                    DropDownList2.Enabled = false;
                    DropDownList3.Enabled = false;
                    Button1.Enabled = false;
                    if (String.IsNullOrEmpty((string)Session["error"]))
                    {
                        Label1.Visible = false;
                    }
                    else
                    {
                        Label1.Visible = true;
                        Label1.ForeColor = System.Drawing.Color.Red;
                        Label1.Text = (string)Session["error"];
                    }
                }
            }
            else if (Request.UrlReferrer == null)
            {
                Response.Redirect("Pagrindinis.aspx");
            }
            else
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Register();

        }

        protected void Register()
        {
            Label1.Visible = true;
            Label1.ForeColor = System.Drawing.Color.Red;
            string id = TextBox1.Text;
            if (String.IsNullOrEmpty(id))
            {
                Session["error"] = "ID turi būti įvestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            int i;
            if(!int.TryParse(id, out i) || int.Parse(id) < 1)
            {
                Session["error"] = "ID turi būti teigimas sveikasis skaičius";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string password = TextBox2.Text;
            if (String.IsNullOrEmpty(password))
            {
                Session["error"] = "Slaptažodis turi būti įvestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if(password.Length < 5)
            {
                Session["error"] = "Slaptažodis per trumpas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string first_name = TextBox3.Text;
            if (String.IsNullOrEmpty(first_name))
            {
                Session["error"] = "Vardas turi būti įvestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Regex.Match(TextBox3.Text, @"[^a-zA-Z]").Success)
            {
                Session["error"] = "Vardas turi būti sudarytas tik iš lotyniškų raidžių";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string last_name = TextBox4.Text;
            if (String.IsNullOrEmpty(last_name))
            {
                Session["error"] = "Pavardė turi būti įvesta";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Regex.Match(TextBox4.Text, @"[^a-zA-Z]").Success)
            {
                Session["error"] = "Pavardė turi būti sudaryta tik iš lotyniškų raidžių";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string bank_account = TextBox5.Text;

            if (String.IsNullOrEmpty(bank_account))
            {
                Session["error"] = "Banko sąskaita turi būti įvesta";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string telephone = TextBox6.Text;
            if (String.IsNullOrEmpty(telephone))
            {
                Session["error"] = "Telefono numeris turi būti įvestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string email = TextBox7.Text;
            if (String.IsNullOrEmpty(email))
            {
                Session["error"] = "Elektroninis paštas turi būti įvestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (email.Count((x) => x.Equals('@')) != 1)
            {
                Session["error"] = "Elektroninis paštas turi būti įvestas teisingai";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (!email.Substring(email.IndexOf('@')).Contains("."))
            {
                Session["error"] = "Elektroninis paštas turi būti įvestas teisingai";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string gender = RadioButtonList1.SelectedValue;
            string time = TextBox8.Text;
            if (String.IsNullOrEmpty(time))
            {
                Session["error"] = "Darbo stažas turi būti įvestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (!int.TryParse(time, out i) || int.Parse(time) < 0)
            {
                Session["error"] = "Darbo stažas turi būti teigiamas sveikasis skaičius";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string date = DropDownList1.SelectedValue + "-" + 
                (DropDownList2.SelectedIndex + 1) + "-" + DropDownList3.SelectedValue;
            Justino_class.Vaistininkas vaistininkas = new Justino_class.Vaistininkas
                (id, first_name, last_name, bank_account, telephone, email, 
                gender, int.Parse(time), date, ((Justino_class.Vaistininkas)Session["vaistininkas"]).Vaistine);
            vaistininkas.Irasyti(password);
            Label1.ForeColor = System.Drawing.Color.Green;
            Label1.Text = "DONE";
            Session["error"] = "";
            Response.Redirect("Vaistininkai.aspx");
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList2.Enabled = true;
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList3.Items.Add("Pasirinkite dieną");
            switch (DropDownList2.SelectedValue)
            {
                case "Sausio":
                case "Kovo":
                case "Gegužės":
                case "Liepos":
                case "Rugpjūčio":
                case "Spalio":
                case "Gruodžio":
                    for (int i = 0; i < 31; i++)
                        DropDownList3.Items.Add((i + 1).ToString());
                    break;
                case "Balandžio":
                case "Birželio":
                case "Rugsėjo":
                case "Lapkričio":
                    for (int i = 0; i < 30; i++)
                        DropDownList3.Items.Add((i + 1).ToString());
                    break;
                case "Vasario":
                    if(int.Parse(DropDownList1.SelectedValue) % 4 == 0)
                        for (int i = 0; i < 29; i++)
                            DropDownList3.Items.Add((i + 1).ToString());
                    else
                        for (int i = 0; i < 28; i++)
                            DropDownList3.Items.Add((i + 1).ToString());
                    break;
            }
            DropDownList3.Enabled = true;
        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            Button1.Enabled = true;
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}