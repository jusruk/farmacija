﻿using IS_project.Shared_class;
using IS_project.Edvino_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace IS_project
{
    public partial class Patalpos : System.Web.UI.Page
    {

        DBConnect db = new DBConnect();
        int vaistines_id;
        protected void Page_Load(object sender, EventArgs e)
        {
            hl.NavigateUrl = "Inventorius.aspx";
            hl2.NavigateUrl = "Patalpos.aspx";
            hl3.NavigateUrl = "Tvarkarastis.aspx";
            hl4.NavigateUrl = "PrSrautai.aspx";
            DropDownList2.Items.Insert(0, new ListItem("nera", "0"));
            DropDownList2.Items.Insert(1, new ListItem("yra", "1"));
            DropDownList3.Items.Insert(0, new ListItem("nera", "0"));
            DropDownList3.Items.Insert(1, new ListItem("yra", "1"));
            if (!IsPostBack)
            {

                using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM vaistine"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = db.connection;
                    db.connection.Open();
                    DropDownList4.DataSource = cmd.ExecuteReader();
                    DropDownList4.DataTextField = "pavadinimas";
                    DropDownList4.DataValueField = "id_vaistine";
                    DropDownList4.DataBind();
                    DropDownList4.Items.Insert(0, new ListItem("pasirinkite vaistine", "0"));
                }

               
                if (String.IsNullOrEmpty((string)Session["error"]))
                {
                    Label1.Visible = false;
                }
                else
                {
                    Label1.Visible = true;
                    Label1.ForeColor = System.Drawing.Color.Red;
                    Label1.Text = (string)Session["error"];
                }

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Register();
            Response.Redirect("Vadybininkai.aspx");
        }

        protected void Register()
        {
            string plotas = TextBox1.Text;
            if (String.IsNullOrEmpty(plotas) || double.Parse(plotas) <= 0)
            {
                Session["error"] = "Plotas negali būti neigiamas ir privalo būt įvestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            string paskirtis = TextBox2.Text;
            if (String.IsNullOrEmpty(paskirtis))
            {
                Session["error"] = "Paskirtis turi būti įvestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }

            string sildymoTipas = TextBox3.Text;
            if (String.IsNullOrEmpty(paskirtis))
            {
                Session["error"] = "Paskirtis turi būti įvestas";
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            int signalizacija = int.Parse(DropDownList2.SelectedItem.Value);
            int dumu_detektorius = int.Parse(DropDownList3.SelectedItem.Value);
            vaistines_id = int.Parse(DropDownList4.SelectedItem.Value);
            if (vaistines_id == 0)
            {
                Session["error"] = "Vaistinė turi būti pasirinkta";
                Response.Redirect(Request.UrlReferrer.ToString());
            }




            Edvino_class.Vadybininkas vadybinikas = (Edvino_class.Vadybininkas)Session["vadybininkas"];
            Edvino_class.Patalpa patalpa = new Edvino_class.Patalpa(double.Parse(plotas),paskirtis,sildymoTipas,signalizacija,dumu_detektorius,vadybinikas, vaistines_id);
            patalpa.Register();
           
        }

        

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList2.Items.Insert(0, new ListItem("nera", "0"));
            DropDownList2.Items.Insert(1, new ListItem("yra", "1"));
        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList3.Items.Insert(0, new ListItem("nera", "0"));
            DropDownList3.Items.Insert(1, new ListItem("yra", "1"));
        }

        protected void DropDownList4_SelectedIndexChanged(object sender, EventArgs e)
        {

            using (MySqlCommand cmd = new MySqlCommand("SELECT * FROM vaistine"))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Connection = db.connection;
                db.connection.Open();
                DropDownList4.DataSource = cmd.ExecuteReader();
                DropDownList4.DataTextField = "pavadinimas";
                DropDownList4.DataValueField = "id_vaistine";
                DropDownList4.DataBind();
                DropDownList4.Items.Insert(0, new ListItem("pasirinkite vaistine", "0"));
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }


    }
}