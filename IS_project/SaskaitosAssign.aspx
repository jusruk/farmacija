﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SaskaitosAssign.aspx.cs" Inherits="IS_project.SaskaitosAssign" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
    <style>

    </style>
</head>
<body>
     <form id="form1" runat="server">
       <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="Vaistininkai.aspx">Farmacijos sistema</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Vaistininkai.aspx">Home</a></li>
                    <li ><asp:HyperLink id="hl" runat="server" Text="Registruoti vaistininką"/></li>
                    <li ><asp:HyperLink id="hl1" runat="server" Text="Sudaryti sąskaitą"/></li>
                    <li><asp:HyperLink id="hl2" runat="server" Text="Teikti nuolaidą prekei"/></li>
                    <li class="active"><asp:HyperLink id="hl3" runat="server" Text="Susieti sąskaitą su mokėjimu"/></li>
                    <li><asp:HyperLink ID="hl4" runat="server" Text="Neapmokėtų sąskaitų ataskaita"/></li>
                    <li><asp:HyperLink ID="hl5" runat="server" Text="Permokėtų sąskaitų ataskaita"/></li>
                 <li>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>
                </ul>
  </div>
</nav>
        <div class="container">
            <h2><center>Sąskaitos susiejimas su mokėjimu</center></h2><br />

             <div class="form-group">
                <div class="col-md-10">
            <asp:Label ID="Label1" runat="server"></asp:Label>
            <asp:Label ID="Label2" runat="server" ForeColor="Green" Text="Nesusietų sąskaitų nėra!"></asp:Label>
            <br />
            <asp:Label ID="Label3" runat="server" Text="Sąskaita"></asp:Label>
            <br />
                    </div>
                 </div>
            <div class="form-group">
                <div class="col-md-10">
            <asp:DropDownList ID="DropDownList1" runat="server"  CssClass="form-control" Width ="200px">
            </asp:DropDownList>
            <br />
                </div>
                </div>
            <br />
             <div class="form-group">
                <div class="col-md-10">
            <asp:Label ID="Label4" runat="server" Text="Mokėjimas"></asp:Label>
            <br />
            <asp:DropDownList ID="DropDownList2" runat="server"  CssClass="form-control" Width ="200px">
            </asp:DropDownList>
            <br />
            <br />
                     <asp:Button ID="Button1" runat="server" OnClick="Button1_Click"  CssClass="btn btn-primary" Text="Susieti" />
                    </div>
                 </div>
           
        </div>
    </form>
</body>
</html>
