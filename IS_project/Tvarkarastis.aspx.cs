﻿using IS_project.Shared_class;
using IS_project.Edvino_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace IS_project
{
    public partial class Tvarkarastis : System.Web.UI.Page
    {
        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {
            hl.NavigateUrl = "Inventorius.aspx";
            hl2.NavigateUrl = "Patalpos.aspx";
            hl3.NavigateUrl = "Tvarkarastis.aspx";
            hl4.NavigateUrl = "PrSrautai.aspx";
            if (!IsPostBack)
            {
                DropDownList2.Items.Insert(0, new ListItem("Visi susitikimai", "0"));
                DropDownList2.Items.Insert(1, new ListItem("Šios dienos susitikimai", "1"));
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            db.connection.Open();
            string filter = DropDownList2.SelectedItem.Value;
            int filtras = -1;
            //string finalFilter = null;
            if (filter.Length == 1)
            {
                filtras = int.Parse(filter);
            }
            filteris(filtras);
        }
        protected void filteris(int filtras)
        {
            if (filtras == 0)
            {
                
                string query = "select data,laikas,tyrimas,vardas,pavarde from apziura,klientas where apziura.fk_Klientasid_Klientas = klientas.id_klientas and apziura.data >= CURDATE()  order by data asc, laikas asc";
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                cmd.CommandType = CommandType.Text;

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    GridView1.DataSource = dr;
                    GridView1.DataBind();
                }
            }
            if (filtras == 1)
            {
                string query = "select data,laikas,tyrimas,vardas,pavarde from apziura,klientas where apziura.fk_Klientasid_Klientas = klientas.id_klientas and apziura.data = CURDATE()  order by laikas asc";
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                cmd.CommandType = CommandType.Text;

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    GridView1.DataSource = dr;
                    GridView1.DataBind();
                }
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Data";
                e.Row.Cells[1].Text = "Laikas";
                e.Row.Cells[2].Text = "Tyrimas";
                e.Row.Cells[3].Text = "Vardas";
                e.Row.Cells[4].Text = "Pavardė";
            }
            if(e.Row.Cells[0].Text.Length> 10)
            {
                e.Row.Cells[0].Text=  e.Row.Cells[0].Text.Substring(0,10);
            }
            if (e.Row.Cells[4].Text == "0")
            {
                e.Row.Cells[4].Text = "nėra";
            }
            else if (e.Row.Cells[4].Text == "1")
            {
                e.Row.Cells[4].Text = "yra";
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}
