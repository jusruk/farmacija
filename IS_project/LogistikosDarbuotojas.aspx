﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogistikosDarbuotojas.aspx.cs" Inherits="IS_project.LogistikosDarbuotojas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>

    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="LogistikosDarbuotojas.aspx">Farmacijos sistema</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="LogistikosDarbuotojas.aspx">Home</a></li>
                    <li><asp:HyperLink id="hl" runat="server" Text="Registruoti vairuotoja"/>
                     <li><asp:HyperLink ID="hl2" runat="server">Registruoti transporto priemone</asp:HyperLink></li>  
                     <li><asp:HyperLink ID="hl3" runat="server">Priskirti transporto priemones</asp:HyperLink></li>  
                     <li>   <asp:HyperLink ID="hl4" runat="server">Priskirti uzsakymus</asp:HyperLink></li>  
                 <li>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <h3><center>Profilio informacija</center></h3>
            <p>
                
                <center>vardas:<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></center>
            </p>
        </div>
        <br />
        <br />
        <br />

       <center><h2>Registruoti vairuotojai</h2> <asp:GridView ID="GridView1" runat="server" onrowdatabound="GridView1_RowDataBound">
        </asp:GridView>
           </center>
        <p>
            <br />
            <center><h3>Registruotos transporto priemones</h3></p>
        <asp:GridView ID="GridView2" runat="server" onrowdatabound="GridView2_RowDataBound">
        </asp:GridView></center>
    </form>
</body>
</html>
