﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Inventorius.aspx.cs" Inherits="IS_project.Inventorius" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type<a href="Inventorius.aspx">Inventorius.aspx</a>="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
    <style>

        .form-inline {
            display:inline-block;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
       <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="Vadybininkai.aspx">Farmacijos sistema</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="Vadybininkai.aspx">Home</a></li>
      <li><asp:HyperLink id="hl" runat="server" Text="Registruoti inventorių"/></li>
      <li><asp:HyperLink id="hl2" runat="server" Text="Teikti patalpas"/></li>
        <li><asp:HyperLink id="hl3" runat="server" Text="Sudaryti vizitų tvarkaraštį"/></li>
        <li><asp:HyperLink id="hl4" runat="server" Text="Stebėti prekių srautą"/></li>
         <li>
           <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>

    </ul>
  </div>
</nav>
        <div class="container">
           
           <h1><center>Naujo inventoriaus registracija</center></h1> <br />
            <br />
             <h3><asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></h3>
            <br />
           
            <div class="form-group">
                <div class="col-md-10">
                     Prekės kodas<br />
                    <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width ="200px"></asp:TextBox>
                </div>
            </div>
            <br />
            <br />
            <div class="form-group">
                <div class="col-md-10">
            Paskirtis<br />
            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Height="103px" Width="370px" TextMode="MultiLine"></asp:TextBox>
                </div>
            </div>
            <br />
            <br />
             <div class="form-group">
                <div class="col-md-10">
                        Kiekis<br />
                        <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" Width ="200px"></asp:TextBox>
                        </div>
            </div>
            <br />
            <br />
             <div class="form-group">
                <div class="col-md-10">
            Būklė<br />
            <asp:TextBox ID="TextBox4" runat="server" CssClass="form-control" Width ="200px"></asp:TextBox>
                         </div>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            
          
             <div class="form-line">
                <div class="col-md-6">
                      Pagaminimo data<br />
            <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control"  Width ="200px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
            </asp:DropDownList>
                   
            <asp:DropDownList ID="DropDownList3" runat="server" CssClass="form-control"  Width ="200px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList4" runat="server" CssClass="form-control"  Width ="200px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList4_SelectedIndexChanged">
            </asp:DropDownList>
                     </div>
              

            <br />
            
            <div class="col-md-7">
                    <br />
            Garantinio data<br />
            <asp:DropDownList ID="DropDownList5" runat="server" CssClass="form-control"  Width ="200px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList5_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList6" runat="server" CssClass="form-control"  Width ="200px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList6_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList7" runat="server" CssClass="form-control"  Width ="200px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList7_SelectedIndexChanged">
            </asp:DropDownList>
                         </div>
         
            <br />
            <br />
       
         <div class="col-md-8">
                    <br />
            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control"   Width="257px" Height="46px">
                <asp:ListItem Value="0">-- pasirinkite patalpa --</asp:ListItem> 
            </asp:DropDownList>
             
             
            <br />
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" OnClick="Button1_Click" Text="Registruoti" />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
            <br />
                  </div>
        </div>
    </form>
</body>
</html>