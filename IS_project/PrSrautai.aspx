﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrSrautai.aspx.cs" Inherits="IS_project.PrSrautai" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
</head>
<body>
    <form id="form1" runat="server">
       <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="Vadybininkai.aspx">Farmacijos sistema</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="Vadybininkai.aspx">Home</a></li>
      <li><asp:HyperLink id="hl" runat="server" Text="Registruoti inventorių"/></li>
      <li><asp:HyperLink id="hl2" runat="server" Text="Teikti patalpas"/></li>
        <li><asp:HyperLink id="hl3" runat="server" Text="Sudaryti vizitų tvarkaraštį"/></li>
        <li><asp:HyperLink id="hl4" runat="server" Text="Stebėti prekių srautą"/></li>
         <li>
           <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>

    </ul>
  </div>
</nav>
        <div class="container">
            <h1><center>Ataskaitos apie prekes</center></h1><br />
           <center><br />
               <div class="form-group">
                <div class="col-md-12">
           <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control" Width ="200px" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" AutoPostBack="True">
            </asp:DropDownList>
                    <br />

               <asp:Button ID="Button1" runat="server" Text="filtruoti" CssClass="btn btn-primary" OnClick="Button1_Click" />
                    
                    </center> 
            </div>
        </div>
            <br />
        </div>
        <center><asp:GridView ID="GridView1" runat="server" onrowdatabound="GridView1_RowDataBound">
        </asp:GridView>
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </center>
    </form>
    </body>
</html>
