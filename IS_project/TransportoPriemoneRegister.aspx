﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransportoPriemoneRegister.aspx.cs" Inherits="IS_project.TransportoPriemoneRegister" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="LogistikosDarbuotojas.aspx">Farmacijos sistema</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="LogistikosDarbuotojas.aspx">Home</a></li>
                    <li ><asp:HyperLink id="hl" runat="server" Text="Registruoti vairuotoja"/></li>
                     <li class="active"><asp:HyperLink ID="hl2" runat="server">Registruoti transporto priemone</asp:HyperLink></li>  
                     <li><asp:HyperLink ID="hl3" runat="server">Priskirti transporto priemones</asp:HyperLink></li>  
                     <li>   <asp:HyperLink ID="hl4" runat="server">Priskirti uzsakymus</asp:HyperLink></li>  
                  <li>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <h3><center>Naujos transporto priemones registracija</center></h3><br />
            <asp:Label ID="Label1" runat="server" Text="Label" ></asp:Label>
            <br /></div>
                 
             <div class="form-group">
                <div class="col-md-10">
            Valstybinis numeris<br />
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width ="200px"></asp:TextBox>
            <br /></div>
                 </div>
                     <div class="form-group">
                <div class="col-md-10">
            Marke<br />
            <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Width ="200px"></asp:TextBox>
            <br /></div>
                 </div>
                     <div class="form-group">
                <div class="col-md-10">
            Modelis<br />
            <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control" Width ="200px"></asp:TextBox>
            <br /></div>
                 </div>
                     <div class="form-group">
                <div class="col-md-10">
            Pagaminimo metai<br />
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
            </asp:DropDownList>
            <br /></div>
                 </div>
                     <div class="form-group">
                <div class="col-md-10">
            Kuro tipas<br />
            <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                <asp:ListItem Selected="True">Benzinas</asp:ListItem>
                <asp:ListItem>Dyzelinas</asp:ListItem>
                <asp:ListItem>Benzinas / elektra</asp:ListItem>
                <asp:ListItem>Benzinas / dujos</asp:ListItem>
                <asp:ListItem>Kita</asp:ListItem>
            </asp:RadioButtonList></div>
                 </div>
                     <div class="form-group">
                <div class="col-md-10">
            Rida<br />
            <asp:TextBox ID="TextBox6" runat="server" CssClass="form-control" Width ="200px"></asp:TextBox>
            <br /></div>
                 </div>
                     <div class="form-group">
                <div class="col-md-10">
            Kebulo tipas<br />
            <asp:RadioButtonList ID="RadioButtonList2" runat="server">
                <asp:ListItem Selected="True">Sedanas</asp:ListItem>
                <asp:ListItem>Hecebkas</asp:ListItem>
                <asp:ListItem>Universalas</asp:ListItem>
                <asp:ListItem>Visureigis</asp:ListItem>
                <asp:ListItem>Kita</asp:ListItem>
            </asp:RadioButtonList>
            <br /></div>
                 </div>
                     <div class="form-group">
                <div class="col-md-10">
            Pavaru dezes tipas<br />
            <asp:RadioButtonList ID="RadioButtonList3" runat="server">
                <asp:ListItem Selected="True">Automatine</asp:ListItem>
                <asp:ListItem>Mechanine</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" CssClass="btn btn-primary" Text="Registruoti" />
            <br />
                    <br />

                    <br />
                </div>
                 </div>
      
    </form>
</body>
</html>
