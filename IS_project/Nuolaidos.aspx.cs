﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IS_project.Shared_class;
using System.Data.SqlClient;
using System.Data;
using MySql.Data.MySqlClient;
using IS_project.Justino_class;
using IS_project.Igno_class;

namespace IS_project
{
    public partial class Nuolaidos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hl.NavigateUrl = "VaistininkaiRegister.aspx";
            hl1.NavigateUrl = "SaskaitosCreate.aspx";
            hl2.NavigateUrl = "Nuolaidos.aspx";
            hl3.NavigateUrl = "SaskaitosAssign.aspx";
            hl4.NavigateUrl = "AtaskaitaNeapmoketos.aspx";
            hl5.NavigateUrl = "AtaskaitaPermoketos.aspx";
            if (((Justino_class.Vaistininkas)Session["vaistininkas"]) != null)
            {
                if (!IsPostBack)
                {
                    List<Nuolaida> nuolaidos = Nuolaida.Nuolaidos();
                    if (nuolaidos.Count != 0)
                    {
                        if (String.IsNullOrEmpty((string)Session["error"]))
                        {
                            Label1.Visible = false;
                        }
                        else
                        {
                            Label1.Visible = true;
                            Label1.ForeColor = System.Drawing.Color.Red;
                            Label1.Text = (string)Session["error"];
                        }
                        DropDownList1.Items.Add("Pasirinkite nuolaidą");
                        foreach (var n in nuolaidos)
                            DropDownList1.Items.Add(n.Tipas);
                    }
                    List<Preke> prekes = Preke.Prekes();
                    if (prekes.Count != 0)
                    {
                        if (String.IsNullOrEmpty((string)Session["error"]))
                        {
                            Label1.Visible = false;
                        }
                        else
                        {
                            Label1.Visible = true;
                            Label1.ForeColor = System.Drawing.Color.Red;
                            Label1.Text = (string)Session["error"];
                        }
                        DropDownList2.Items.Add("Pasirinkite prekę");
                        foreach (var p in prekes)
                            DropDownList2.Items.Add(p.pavadinimas);
                    }
                }
                else if (Nuolaida.Nuolaidos().Count == 0 || Preke.Prekes().Count == 0)
                {
                    Label1.Visible = true;
                    Button1.Enabled = false;
                }
                else
                {
                    Label1.Visible = false;
                }
            }
            else if (Request.UrlReferrer == null)
            {
                Response.Redirect("Pagrindinis.aspx");
            }
            else
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<Nuolaida> nuolaidos = Nuolaida.Nuolaidos();
            List<Preke> prekes = Preke.Prekes();
            if (DropDownList1.SelectedIndex != 0 && DropDownList2.SelectedIndex != 0)
            {
                Preke p = prekes[DropDownList2.SelectedIndex - 1];
                Nuolaida n = nuolaidos[DropDownList1.SelectedIndex - 1];
                p.Priskirti(n);
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}