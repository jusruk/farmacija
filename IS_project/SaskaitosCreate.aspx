﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SaskaitosCreate.aspx.cs" Inherits="IS_project.SaskaitosCreate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
    <style>

    </style>
</head>
<body>
     <form id="form1" runat="server">
       <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="Vaistininkai.aspx">Farmacijos sistema</a>
    </div>
    <ul class="nav navbar-nav">
      <li ><a href="Vaistininkai.aspx">Home</a></li>
                    <li ><asp:HyperLink id="hl" runat="server" Text="Registruoti vaistininką"/></li>
                    <li class="active"><asp:HyperLink id="hl1" runat="server" Text="Sudaryti sąskaitą"/></li>
                    <li><asp:HyperLink id="hl2" runat="server" Text="Teikti nuolaidą prekei"/></li>
                    <li><asp:HyperLink id="hl3" runat="server" Text="Susieti sąskaitą su mokėjimu"/></li>
                    <li><asp:HyperLink ID="hl4" runat="server" Text="Neapmokėtų sąskaitų ataskaita"/></li>
                    <li><asp:HyperLink ID="hl5" runat="server" Text="Permokėtų sąskaitų ataskaita"/></li>
                 <li>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>
                </ul>
  </div>
</nav>
        <div class="container">
             <h2><center>Sąskaitos sudarymas</center></h2><br />
            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Krepšelių nėra!"></asp:Label>
            <br />
               <div class="form-group">
                <div class="col-md-10">
           Sąskaitos numeris<br />
            <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width ="200px"></asp:TextBox>
            <br />
                     </div>
            </div>

                       <div class="form-group">
                <div class="col-md-10">
            Tipas<asp:RadioButtonList ID="RadioButtonList1" runat="server" Width ="200px">
                <asp:ListItem Selected="True" Value="0">Sąskaita-faktūra</asp:ListItem>
                <asp:ListItem Value="1">PVM sąskaita-faktūra</asp:ListItem>
            </asp:RadioButtonList>
            <br />
                     </div>
            </div>

                       <div class="form-group">
                <div class="col-md-10">
            Ar apmokėta?<asp:RadioButtonList ID="RadioButtonList2" runat="server" Width ="200px">
                <asp:ListItem Selected="True" Value="0">Ne</asp:ListItem>
                <asp:ListItem Value="1">Taip</asp:ListItem>
            </asp:RadioButtonList>
            <br />
                     </div>
            </div>
             <div class="form-group">
                <div class="col-md-10">
            Apmokėjimo data<br />
            <asp:Calendar ID="Calendar2" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px">
                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                <NextPrevStyle VerticalAlign="Bottom" />
                <OtherMonthDayStyle ForeColor="#808080" />
                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                <SelectorStyle BackColor="#CCCCCC" />
                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                <WeekendDayStyle BackColor="#FFFFCC" />
            </asp:Calendar>
            <br />
                              </div>
            </div>

                       <div class="form-group">
                <div class="col-md-10">
            Prekių krepšelis<br />
            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" CssClass="form-control" Width ="200px">
            </asp:DropDownList>
            <br />
                   
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" CssClass="btn btn-primary" Text="Sudaryti sąskaitą" />
                    <br />
                    <br />
        </div>
                             </div>
            </div>
    </form>
</body>
</html>
