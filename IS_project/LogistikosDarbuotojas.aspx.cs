﻿using IS_project.Shared_class;
using IS_project.Ricardo_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace IS_project
{
    public partial class LogistikosDarbuotojas : System.Web.UI.Page
     {
        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (((Ricardo_class.LogistikosDarbuotojas)Session["LogistikosDarbuotojas"]) != null)
            {
                Ricardo_class.LogistikosDarbuotojas logistikosDarbuotojas = ((Ricardo_class.LogistikosDarbuotojas)Session["LogistikosDarbuotojas"]);
                Label1.Text = logistikosDarbuotojas.Vardas + " " + logistikosDarbuotojas.Pavarde;
                hl.NavigateUrl = "VairuotojaiRegister.aspx";
                hl2.NavigateUrl = "TransportoPriemoneRegister.aspx";
                hl3.NavigateUrl = "TransportoPriemoneAssign.aspx";
                hl4.NavigateUrl = "UzsakymaiAssign.aspx";

                db.connection.Open();
                string query = "select vardas, pavarde, pazymejimo_nr, imone, tel_nr, el_pastas from vairuotojas" +
                    " WHERE vairuotojas.pazymejimo_nr != 555";
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                cmd.CommandType = CommandType.Text;

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    GridView1.DataSource = dr;
                    GridView1.DataBind();
                }

                string query2 = "select valstybinis_nr, marke, modelis, pagaminimo_metai, kuras, rida, kebulas, pavaru_deze from transporto_priemone";
                MySqlCommand cmd2 = new MySqlCommand(query2, db.connection);
                cmd2.CommandType = CommandType.Text;

                using (MySqlDataReader dr = cmd2.ExecuteReader())
                {
                    GridView2.DataSource = dr;
                    GridView2.DataBind();
                }
            }
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].Text = "Vardas";
                    e.Row.Cells[1].Text = "Pavarde";
                    e.Row.Cells[2].Text = "Pazymejimo numeris";
                    e.Row.Cells[3].Text = "Atstovaujama imone";
                    e.Row.Cells[4].Text = "Telefono numeris";
                    e.Row.Cells[5].Text = "Elektroninis pastas";
                }
            }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Valstybiniai numeriai";
                e.Row.Cells[1].Text = "Marke";
                e.Row.Cells[2].Text = "Modelis";
                e.Row.Cells[3].Text = "Pagaminimo metai";
                e.Row.Cells[4].Text = "Kuro tipas";
                e.Row.Cells[5].Text = "Rida";
                e.Row.Cells[6].Text = "Kebulo tipas";
                e.Row.Cells[7].Text = "Pavaru dezes tipas";
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}