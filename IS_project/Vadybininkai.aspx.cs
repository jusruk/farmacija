﻿using IS_project.Shared_class;
using IS_project.Edvino_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;


namespace IS_project
{
    public partial class Vadybininkai : System.Web.UI.Page
    {

        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (((Edvino_class.Vadybininkas)Session["vadybininkas"]) != null)
            {
                Edvino_class.Vadybininkas vadybininkas = ((Edvino_class.Vadybininkas)Session["vadybininkas"]);
                Label1.Text = vadybininkas.Vardas + " " + vadybininkas.Pavarde;
                hl.NavigateUrl = "Inventorius.aspx";
                hl2.NavigateUrl = "Patalpos.aspx";
                hl3.NavigateUrl = "Tvarkarastis.aspx";
                hl4.NavigateUrl = "PrSrautai.aspx";
               
                

                db.connection.Open();
                string query = "select prekes_kodas, paskirtis, kiekis, bukle from inventorius";
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                cmd.CommandType = CommandType.Text;

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    GridView1.DataSource = dr;
                    GridView1.DataBind();
                }

                string queryy = "select pavadinimas,id_Patalpa,plotas, paskirtis, sildymo_tipas from patalpa,vaistine where patalpa.fk_Vaistineid_Vaistine = vaistine.id_vaistine";
                MySqlCommand cmdd = new MySqlCommand(queryy, db.connection);
                cmdd.CommandType = CommandType.Text;

                using (MySqlDataReader dr = cmdd.ExecuteReader())
                {
                    GridView2.DataSource = dr;
                    GridView2.DataBind();
                }

                List<Vaistine> vaistines = Vaistine.GautiVaistines();
                GridView3.DataSource = vaistines;
                GridView3.DataBind();

            }

        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Inventoriaus kodas";
                e.Row.Cells[1].Text = "Paskirtis";
                e.Row.Cells[2].Text = "Kiekis";
                e.Row.Cells[3].Text = "Bukle";
            }
        }
        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Vaistinė";
                e.Row.Cells[1].Text = "Patalpa";
                e.Row.Cells[2].Text = "Plotas";
                e.Row.Cells[3].Text = "Paskirtis";
                e.Row.Cells[4].Text = "Šildymo tipas";
            }
        }
        protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Pavadinimas";
                e.Row.Cells[1].Text = "Adresas";
                e.Row.Cells[2].Text = "Telefonas";
                e.Row.Cells[3].Text = "El. paštas";
                e.Row.Cells[4].Text = "Faksas";
                e.Row.Cells[5].Text = "Pašto kodas";
                e.Row.Cells[6].Text = "Miestas";
                e.Row.Cells[7].Text = "Dydis aukštais";
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}