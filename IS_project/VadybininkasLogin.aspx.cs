﻿using IS_project.Shared_class;
using IS_project.Edvino_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IS_project
{
    public partial class VadybininkasLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {

            }
            else
            {
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            
            DBConnect db = new DBConnect();
            db.connection.Open();

            MySqlCommand cmd = db.connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select * from vadybininkas where tabelio_nr= '" + t1.Text + "' and password='" + t2.Text + "'";
            cmd.ExecuteNonQuery();
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(cmd);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Session["vadybininkas"] = new Edvino_class.Vadybininkas(
                  dr["tabelio_nr"].ToString(),
                  dr["vardas"].ToString(),
                  dr["pavarde"].ToString(),
                  dr["tel_nr"].ToString(),
                  dr["el_pastas"].ToString(),
                  dr["faksas"].ToString(),
                  dr["id_vadybininkas"].ToString());

                Response.Redirect("Vadybininkai.aspx", false);
            }
            db.connection.Close();


        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["Vadybininkas"] = null;
        }
        protected void Session_End(object sender, EventArgs e)
        {

        }
    }
}