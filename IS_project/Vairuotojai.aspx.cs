﻿using IS_project.Shared_class;
using IS_project.Ricardo_class;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

namespace IS_project
{
    public partial class Vairuotojai : System.Web.UI.Page
    {
        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (((Ricardo_class.Vairuotojas)Session["Vairuotojas"]) != null)
            {
                Ricardo_class.Vairuotojas vairuotojas = ((Ricardo_class.Vairuotojas)Session["Vairuotojas"]);
                Label1.Text = vairuotojas.Pavarde;

                db.connection.Open();
                string query = "select uzsakymo_id from uzsakymas" +
                    " WHERE fk_Vairuotojaspazymejimo_nr = '" + vairuotojas.PazymejimoNr + "'";
                MySqlCommand cmd = new MySqlCommand(query, db.connection);
                cmd.CommandType = CommandType.Text;

                using (MySqlDataReader dr = cmd.ExecuteReader())
                {
                    GridView1.DataSource = dr;
                    GridView1.DataBind();
                }

                string query2 = "select valstybinis_nr, marke, modelis, pagaminimo_metai, kuras, rida, kebulas, pavaru_deze" +
                    " from transporto_priemone WHERE fk_Vairuotojaspazymejimo_nr = '" + vairuotojas.PazymejimoNr + "'";
                MySqlCommand cmd2 = new MySqlCommand(query2, db.connection);
                cmd2.CommandType = CommandType.Text;

                using (MySqlDataReader dr = cmd2.ExecuteReader())
                {
                    GridView2.DataSource = dr;
                    GridView2.DataBind();
                }


                string query3 = "SELECT DISTINCT pavadinimas, adresas, tiekejas.tel_nr, miestas from vairuotojas, tiekejas, uzsakymas " +
                    "WHERE '" + vairuotojas.PazymejimoNr + "' = uzsakymas.fk_Vairuotojaspazymejimo_nr " +
                    "AND uzsakymas.fk_Tiekejasid_Tiekejas = tiekejas.id_tiekejas";
                MySqlCommand cmd3 = new MySqlCommand(query3, db.connection);
                cmd3.CommandType = CommandType.Text;

                using (MySqlDataReader dr = cmd3.ExecuteReader())
                {
                    GridView3.DataSource = dr;
                    GridView3.DataBind();
                }
            }
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Uzsakymo ID";
            }
        }

        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Valstybiniai numeriai";
                e.Row.Cells[1].Text = "Marke";
                e.Row.Cells[2].Text = "Modelis";
                e.Row.Cells[3].Text = "Pagaminimo metai";
                e.Row.Cells[4].Text = "Kuro tipas";
                e.Row.Cells[5].Text = "Rida";
                e.Row.Cells[6].Text = "Kebulo tipas";
                e.Row.Cells[7].Text = "Pavaru dezes tipas";
            }
        }

        protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[0].Text = "Pavadinimas";
                e.Row.Cells[1].Text = "Adresas";
                e.Row.Cells[2].Text = "Telefono numeris";
                e.Row.Cells[3].Text = "Miestas";
            }
        }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Pagrindinis.aspx");
            Session.Abandon();
        }
    }
}