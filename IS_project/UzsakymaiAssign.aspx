﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UzsakymaiAssign.aspx.cs" Inherits="IS_project.UzsakymaiAssign" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <environment names="Development">
        <link rel="stylesheet" href="~/lib/bootstrap/dist/css/bootstrap.css" />
        <link rel="stylesheet" href="~/css/site.css" />
         <link rel="stylesheet" type="text/css" href="style.css" />

    </environment>
    <environment names="Staging,Production">
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/3.3.6/css/bootstrap.min.css"
              asp-fallback-href="~/lib/bootstrap/dist/css/bootstrap.min.css"
              asp-fallback-test-class="sr-only" asp-fallback-test-property="position" asp-fallback-test-value="absolute" />
        <link rel="stylesheet" href="~/css/site.min.css" asp-append-version="true" />
    </environment>
</head>
<body>
    <form id="form1" runat="server">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="LogistikosDarbuotojas.aspx">Farmacijos sistema</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="LogistikosDarbuotojas.aspx">Home</a></li>
                    <li ><asp:HyperLink id="hl" runat="server" Text="Registruoti vairuotoja"/></li>
                     <li><asp:HyperLink ID="hl2" runat="server">Registruoti transporto priemone</asp:HyperLink></li>  
                     <li ><asp:HyperLink ID="hl3" runat="server">Priskirti transporto priemones</asp:HyperLink></li>  
                     <li class="active">   <asp:HyperLink ID="hl4" runat="server">Priskirti uzsakymus</asp:HyperLink></li>  
                  <li>
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Atsijungti</asp:LinkButton></li>
                </ul>
            </div>
        </nav>

        <div class="container">
           <h2><center>Uzsakymo priskyrimas vairuotojui</center></h2><br />
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <br />
            <br />
              <div class="form-group">
                <div class="col-md-10">
            Uzsakymas<br />
            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" Width ="200px" AutoPostBack="True">
            </asp:DropDownList>
            <br />
                    </div>
                  </div>
                      <div class="form-group">
                <div class="col-md-10">
            Vairuotojas<br />
            <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control" Width ="200px" AutoPostBack="True">
            </asp:DropDownList>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="Priskirti" CssClass="btn btn-primary" OnClick="Button1_Click"/>
        </div>
                          </div>
            </div>
    </form>
</body>
</html>
